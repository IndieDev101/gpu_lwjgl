package com.indiedev101;

public class Mathi {
	public static int nextPowerOf2(final int a) {
		int b = 1;
		while(b < a)
			b = b << 1;
		return b;
	}
}