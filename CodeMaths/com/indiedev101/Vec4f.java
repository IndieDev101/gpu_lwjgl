package com.indiedev101;

public class Vec4f {
	public float x;
	public float y;
	public float z;
	public float w;

	public Vec4f() {
	}

	public Vec4f(float x, float y, float z, float w) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
	}

	public Vec4f(float[] v) {
		x = v[0];
		y = v[1];
		z = v[2];
		w = v[3];
	}

	public Vec4f set(float x, float y, float z, float w) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
		return this;
	}

	public Vec4f set(Vec4f v) {
		x = v.x;
		y = v.y;
		z = v.z;
		w = v.w;
		return this;
	}

	public float length() {
		return Mathf.sqrt(x * x + y * y + z * z);
	}
}