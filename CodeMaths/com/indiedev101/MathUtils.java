package com.indiedev101;

import java.nio.FloatBuffer;

public class MathUtils {
	public static Mat4f setLookAt(float eyeX, float eyeY, float eyeZ, float atX, float atY, float atZ, float upX, float upY, float upZ, Mat4f result) {
		float fx = eyeX - atX;
		float fy = eyeY - atY;
		float fz = eyeZ - atZ;
		float ft = 1.0f / (float)Math.sqrt(fx * fx + fy * fy + fz * fz);
		fx *= ft;
		fy *= ft;
		fz *= ft;

		float sx = upY * fz - upZ * fy;
		float sy = upZ * fx - upX * fz;
		float sz = upX * fy - upY * fx;
		float st = 1.0f / (float)Math.sqrt(sx * sx + sy * sy + sz * sz);
		sx *= st;
		sy *= st;
		sz *= st;

		float ux = fy * sz - fz * sy;
		float uy = fz * sx - fx * sz;
		float uz = fx * sy - fy * sx;
		float ut = 1.0f / (float)Math.sqrt(ux * ux + uy * uy + uz * uz);
		ux *= ut;
		uy *= ut;
		uz *= ut;

		result.m[3] = 0.0f;
		result.m[7] = 0.0f;
		result.m[11] = 0.0f;
		result.m[15] = 1.0f;

		result.m[0] = sx;
		result.m[4] = sy;
		result.m[8] = sz;

		result.m[1] = ux;
		result.m[5] = uy;
		result.m[9] = uz;

		result.m[2] = fx;
		result.m[6] = fy;
		result.m[10] = fz;

		result.m[12] = -(sx * eyeX + sy * eyeY + sz * eyeZ);
		result.m[13] = -(ux * eyeX + uy * eyeY + uz * eyeZ);
		result.m[14] = -(fx * eyeX + fy * eyeY + fz * eyeZ);

		return result;
	}

	public static Mat4f setPersp(float fovDegrees, float aspectRatio, float nearClip, float farClip, Mat4f result) {
		float fovRadians = (float)Math.toRadians(fovDegrees);
		float focalLength = (float)Math.tan(fovRadians * 0.5f);

		result.m[0] = 1.0f / (focalLength * aspectRatio);
		result.m[1] = 0.0f;
		result.m[2] = 0.0f;
		result.m[3] = 0.0f;

		result.m[4] = 0.0f;
		result.m[5] = 1.0f / focalLength;
		result.m[6] = 0.0f;
		result.m[7] = 0.0f;

		result.m[8] = 0.0f;
		result.m[9] = 0.0f;
		result.m[10] = farClip / (nearClip - farClip);
		result.m[11] = -1.0f;

		result.m[12] = 0.0f;
		result.m[13] = 0.0f;
		result.m[14] = (farClip * nearClip) / (nearClip - farClip);
		result.m[15] = 0.0f;

		return result;
	}

	public static Mat4f setOrtho(float viewWidth, float viewHeight, float nearClip, float farClip, Mat4f result) {
		result.m[0] = 2.0f / viewWidth;
		result.m[1] = 0.0f;
		result.m[2] = 0.0f;
		result.m[3] = 0.0f;

		result.m[4] = 0.0f;
		result.m[5] = 2.0f / viewHeight;
		result.m[6] = 0.0f;
		result.m[7] = 0.0f;

		result.m[8] = 0.0f;
		result.m[9] = 0.0f;
		result.m[10] = 1.0f / (nearClip - farClip);
		result.m[11] = 0.0f;

		result.m[12] = -1.0f;
		result.m[13] = -1.0f;
		result.m[14] = (1.0f / (nearClip - farClip)) * nearClip;
		result.m[15] = 1.0f;

		return result;
	}

	public static void putMatrixInBuffer(Mat4f matrix, FloatBuffer buffer) {
		buffer.clear();
		for(int i = 0; i < 16; i++)
			buffer.put(matrix.m[i]);
		buffer.flip();
		buffer.limit(16);
	}
}