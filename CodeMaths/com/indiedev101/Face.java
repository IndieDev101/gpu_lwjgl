package com.indiedev101;

public class Face {
	public static final int negX = 0;
	public static final int negY = 1;
	public static final int negZ = 2;
	public static final int posX = 3;
	public static final int posY = 4;
	public static final int posZ = 5;

	public static String name(int face) {
		switch(face) {
			case Face.negX:
				return "negX";
			case Face.negY:
				return "negY";
			case Face.negZ:
				return "negZ";
			case Face.posX:
				return "posX";
			case Face.posY:
				return "posY";
			case Face.posZ:
				return "posZ";
			default:
				return "";
		}
	}

	public static int mate(int face) {
		switch(face) {
			case Face.negX:
				return Face.posX;
			case Face.negY:
				return Face.posY;
			case Face.negZ:
				return Face.posZ;
			case Face.posX:
				return Face.negX;
			case Face.posY:
				return Face.negY;
			case Face.posZ:
				return Face.negZ;
			default:
				return -1;
		}
	}
}