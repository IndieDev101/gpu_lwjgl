package com.indiedev101;

public class ARGB {
	public static final int abgr(int c) {
		int r = (c & 0x00ff0000) >> 16;
		int g = (c & 0x0000ff00) >> 8;
		int b = (c & 0x000000ff);
		int a = (c & 0xff000000) >>> 24;
		return a << 24 | b << 16 | g << 8 | r;
	}
}