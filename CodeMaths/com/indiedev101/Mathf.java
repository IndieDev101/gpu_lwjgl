package com.indiedev101;

public class Mathf {
	public static final float PI = (float)Math.PI;
	public static final float PIOVER2 = PI / 2.0f;
	public static final float PIUNDER2 = 2.0f / PI;
	public static final float PIOVER180 = PI / 180.0f;
	public static final float PIUNDER180 = 180.0f / PI;
	public static final float EPSILON = 1.192092896e-07f;

	public static float toRadians(float a) {
		return a * PIOVER180;
	}

	public static float toDegrees(float a) {
		return a * PIUNDER180;
	}

	public static float sin(float a) {
		return (float)Math.sin(a);
	}

	public static float cos(float a) {
		return (float)Math.cos(a);
	}

	public static float tan(float a) {
		return (float)Math.tan(a);
	}

	public static float mod(float a, float b) {
		return a % b;
	}

	public static float sqrt(float a) {
		return (float)Math.sqrt(a);
	}

	public static float clamp(float val, float min, float max) {
		return val < min ? min : (val > max ? max : val);
	}

	public static float ease(float t, float k1, float k2) {
		float f = k1 * PIUNDER2 + k2 - k1 + ((1.0f - k2) * PIUNDER2);
		float s = 0.0f;
		if(t < k1) {
			s = k1 * PIUNDER2 * (sin((t / k1) * PIOVER2 - PIOVER2) + 1.0f);
		} else if(t > k2) {
			s = k1 / PIOVER2 + k2 - k1 + (((1.0f - k2) * PIUNDER2) * sin(((t - k2) / (1.0f - k2)) * PIOVER2));
		} else {
			s = k1 / PIOVER2 + t - k1;
		}
		return s / f;
	}

	public static float getAngleBetweenPosNeg180(float angle) {
		angle = mod(angle, 360.0f);
		if(angle < -180.0f) {
			angle += 360.0f;
		} else if(angle >= 180.0f) {
			angle -= 360.0f;
		}
		return angle;
	}

	public static void setDirectionFromYawAndPitch(Vec3f direction, float yaw, float pitch) {
		float t = cos(-pitch * PIOVER180);
		float x = sin(yaw * PIOVER180);
		float y = sin(pitch * PIOVER180);
		float z = cos(-yaw * PIOVER180);
		direction.set(x * t, y, z * t);
		direction.normalize();
	}
}