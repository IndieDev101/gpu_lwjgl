package com.indiedev101;

import java.util.ArrayList;

public class MatStack4f {
	private int index = 1;
	private ArrayList<Mat4f> stack = new ArrayList<Mat4f>();
	private Mat4f working = new Mat4f();

	public Mat4f matrix = new Mat4f();
	public int dirty = 1;

	public MatStack4f() {
		matrix.setIdentity();
		stack.add(matrix);
	}

	public void reset() {
		index = 1;
		matrix = stack.get(index - 1);
	}

	public void push() {
		Mat4f prev = matrix;
		if(index < stack.size())
			matrix = stack.get(index);
		else
			stack.add(matrix = new Mat4f());
		matrix.set(prev);
		index++;
	}

	public void pop() {
		if(index > 1)
			index--;
		matrix = stack.get(index - 1);
		dirty++;
	}

	public Mat4f back() {
		return matrix;
	}

	public void setIdentity() {
		matrix.setIdentity();
		dirty++;
	}

	public void setTranslate(float x, float y, float z) {
		matrix.setTranslate(x, y, z);
		dirty++;
	}

	public void setRotateX(float degrees) {
		matrix.setRotateX(degrees);
		dirty++;
	}

	public void setRotateY(float degrees) {
		matrix.setRotateY(degrees);
		dirty++;
	}

	public void setRotateZ(float degrees) {
		matrix.setRotateZ(degrees);
		dirty++;
	}

	public void setScale(float x, float y, float z) {
		matrix.setScale(x, y, z);
		dirty++;
	}

	public void mul(Mat4f matrix) {
		this.matrix.mul(matrix);
		dirty++;
	}

	public void mulTranslate(float x, float y, float z) {
		working.setTranslate(x, y, z);
		matrix.mul(working);
		dirty++;
	}

	public void mulRotateX(float degrees) {
		working.setRotateX(degrees);
		matrix.mul(working);
		dirty++;
	}

	public void mulRotateY(float degrees) {
		working.setRotateY(degrees);
		matrix.mul(working);
		dirty++;
	}

	public void mulRotateZ(float degrees) {
		working.setRotateZ(degrees);
		matrix.mul(working);
		dirty++;
	}

	public void mulScale(float x, float y, float z) {
		working.setScale(x, y, z);
		matrix.mul(working);
		dirty++;
	}
}