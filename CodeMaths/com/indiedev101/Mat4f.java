package com.indiedev101;

public class Mat4f {
	public float[] m = new float[16];

	public Mat4f setIdentity() {
		m[1] = m[2] = m[3] = m[4] = m[6] = m[7] = m[8] = m[9] = m[11] = m[12] = m[13] = m[14] = 0;
		m[0] = m[5] = m[10] = m[15] = 1;
		return this;
	}

	public void set(Mat4f mat) {
		for(int i = 0; i < 16; i++)
			m[i] = mat.m[i];
	}

	public void setTranslate(float x, float y, float z) {
		m[0] = 1.0f;
		m[1] = 0.0f;
		m[2] = 0.0f;
		m[3] = 0.0f;
		m[4] = 0.0f;
		m[5] = 1.0f;
		m[6] = 0.0f;
		m[7] = 0.0f;
		m[8] = 0.0f;
		m[9] = 0.0f;
		m[10] = 1.0f;
		m[11] = 0.0f;
		m[12] = x;
		m[13] = y;
		m[14] = z;
		m[15] = 1.0f;
	}

	public void setRotateX(float degrees) {
		float radians = Mathf.toRadians(degrees);
		float sinRadians = Mathf.sin(radians);
		float cosRadians = Mathf.cos(radians);
		m[0] = 1.0f;
		m[1] = 0.0f;
		m[2] = 0.0f;
		m[3] = 0.0f;
		m[4] = 0.0f;
		m[5] = cosRadians;
		m[6] = sinRadians;
		m[7] = 0.0f;
		m[8] = 0.0f;
		m[9] = -sinRadians;
		m[10] = cosRadians;
		m[11] = 0.0f;
		m[12] = 0.0f;
		m[13] = 0.0f;
		m[14] = 0.0f;
		m[15] = 1.0f;
	}

	public void setRotateY(float degrees) {
		float radians = Mathf.toRadians(degrees);
		float sinRadians = Mathf.sin(radians);
		float cosRadians = Mathf.cos(radians);
		m[0] = cosRadians;
		m[1] = 0.0f;
		m[2] = -sinRadians;
		m[3] = 0.0f;
		m[4] = 0.0f;
		m[5] = 1.0f;
		m[6] = 0.0f;
		m[7] = 0.0f;
		m[8] = sinRadians;
		m[9] = 0.0f;
		m[10] = cosRadians;
		m[11] = 0.0f;
		m[12] = 0.0f;
		m[13] = 0.0f;
		m[14] = 0.0f;
		m[15] = 1.0f;
	}

	public void setRotateZ(float degrees) {
		float radians = Mathf.toRadians(degrees);
		float sinRadians = Mathf.sin(radians);
		float cosRadians = Mathf.cos(radians);
		m[0] = cosRadians;
		m[1] = sinRadians;
		m[2] = 0.0f;
		m[3] = 0.0f;
		m[4] = -sinRadians;
		m[5] = cosRadians;
		m[6] = 0.0f;
		m[7] = 0.0f;
		m[8] = 0.0f;
		m[9] = 0.0f;
		m[10] = 1.0f;
		m[11] = 0.0f;
		m[12] = 0.0f;
		m[13] = 0.0f;
		m[14] = 0.0f;
		m[15] = 1.0f;
	}

	public void setScale(float x, float y, float z) {
		m[0] = x;
		m[1] = 0.0f;
		m[2] = 0.0f;
		m[3] = 0.0f;
		m[4] = 0.0f;
		m[5] = y;
		m[6] = 0.0f;
		m[7] = 0.0f;
		m[8] = 0.0f;
		m[9] = 0.0f;
		m[10] = z;
		m[11] = 0.0f;
		m[12] = 0.0f;
		m[13] = 0.0f;
		m[14] = 0.0f;
		m[15] = 1.0f;
	}

	public void mul(Mat4f mat) {
		float[] a = m;
		float[] b = mat.m;
		float a0 = a[0];
		float a1 = a[1];
		float a2 = a[2];
		float a3 = a[3];
		float a4 = a[4];
		float a5 = a[5];
		float a6 = a[6];
		float a7 = a[7];
		float a8 = a[8];
		float a9 = a[9];
		float a10 = a[10];
		float a11 = a[11];
		float a12 = a[12];
		float a13 = a[13];
		float a14 = a[14];
		float a15 = a[15];
		float b0 = b[0];
		float b1 = b[1];
		float b2 = b[2];
		float b3 = b[3];
		float b4 = b[4];
		float b5 = b[5];
		float b6 = b[6];
		float b7 = b[7];
		float b8 = b[8];
		float b9 = b[9];
		float b10 = b[10];
		float b11 = b[11];
		float b12 = b[12];
		float b13 = b[13];
		float b14 = b[14];
		float b15 = b[15];
		m[0] = a0 * b0 + a4 * b1 + a8 * b2 + a12 * b3;
		m[4] = a0 * b4 + a4 * b5 + a8 * b6 + a12 * b7;
		m[8] = a0 * b8 + a4 * b9 + a8 * b10 + a12 * b11;
		m[12] = a0 * b12 + a4 * b13 + a8 * b14 + a12 * b15;
		m[1] = a1 * b0 + a5 * b1 + a9 * b2 + a13 * b3;
		m[5] = a1 * b4 + a5 * b5 + a9 * b6 + a13 * b7;
		m[9] = a1 * b8 + a5 * b9 + a9 * b10 + a13 * b11;
		m[13] = a1 * b12 + a5 * b13 + a9 * b14 + a13 * b15;
		m[2] = a2 * b0 + a6 * b1 + a10 * b2 + a14 * b3;
		m[6] = a2 * b4 + a6 * b5 + a10 * b6 + a14 * b7;
		m[10] = a2 * b8 + a6 * b9 + a10 * b10 + a14 * b11;
		m[14] = a2 * b12 + a6 * b13 + a10 * b14 + a14 * b15;
		m[3] = a3 * b0 + a7 * b1 + a11 * b2 + a15 * b3;
		m[7] = a3 * b4 + a7 * b5 + a11 * b6 + a15 * b7;
		m[11] = a3 * b8 + a7 * b9 + a11 * b10 + a15 * b11;
		m[15] = a3 * b12 + a7 * b13 + a11 * b14 + a15 * b15;
	}
}