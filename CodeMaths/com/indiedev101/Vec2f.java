package com.indiedev101;

public class Vec2f {
	public float x;
	public float y;

	public Vec2f() {
	}

	public Vec2f(float x, float y) {
		this.x = x;
		this.y = y;
	}

	public Vec2f(float[] v) {
		x = v[0];
		y = v[1];
	}

	public void set(float x, float y) {
		this.x = x;
		this.y = y;
	}

	public Vec2f set(Vec2f v) {
		x = v.x;
		y = v.y;
		return this;
	}

	public float length() {
		return Mathf.sqrt(x * x + y * y);
	}
}