package com.indiedev101;

public class Vec3f {
	public float x;
	public float y;
	public float z;

	public Vec3f() {
	}

	public Vec3f(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public Vec3f(float[] v) {
		x = v[0];
		y = v[1];
		z = v[2];
	}

	public Vec3f set(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
		return this;
	}

	public Vec3f set(Vec3f v) {
		x = v.x;
		y = v.y;
		z = v.z;
		return this;
	}

	public float length() {
		return Mathf.sqrt(x * x + y * y + z * z);
	}

	public Vec3f normalize() {
		float l = length();
		if(l > Mathf.EPSILON) {
			float rl = 1.0f / l;
			x *= rl;
			y *= rl;
			z *= rl;
		}
		return this;
	}

	public Vec3f mul(float f) {
		x *= f;
		y *= f;
		z *= f;
		return this;
	}

	public void add(Vec3f a, Vec3f b) {
		x = a.x + b.x;
		y = a.y + b.y;
		z = a.z + b.z;
	}
}