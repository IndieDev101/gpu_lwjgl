package com.indiedev101;

public class SampleTemplate extends Sample3D {
	@Override
	public void onCreate() {
	}

	@Override
	public void onDestroy() {
	}

	@Override
	public void onResize(int width, int height) {
	}

	@Override
	public void onVariableUpdate(float timeStep) {
	}

	@Override
	public void onFixedUpdate(float timeStep) {
	}

	@Override
	public void onRender3D() {
		Grid.instance.render();
	}

	@Override
	public void onRender2D() {
		renderInfo();
	}

	public boolean onMouseDown(int button, int x, int y) {
		return false;
	}

	@Override
	public void onMouseMove(int mx, int my, int px, int py) {
	}

	@Override
	public void onMouseUp(int button, int x, int y) {
	}

	@Override
	public void onKeyDown(int key) {
	}

	@Override
	public void onKeyUp(int key) {
	}
}