package com.indiedev101;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

import org.lwjgl.BufferUtils;

public class DataBuffer {
	public int sizeInBytes;
	public ByteBuffer byteBuffer;
	public ShortBuffer shortBuffer;
	public IntBuffer intBuffer;
	public FloatBuffer floatBuffer;
	public int ticksUntilFree;

	public void create(int numBytes) {
		sizeInBytes = numBytes;
		byteBuffer = BufferUtils.createByteBuffer(numBytes);
		shortBuffer = byteBuffer.asShortBuffer();
		intBuffer = byteBuffer.asIntBuffer();
		floatBuffer = byteBuffer.asFloatBuffer();
	}

	public void destroy() {
		floatBuffer = null;
		intBuffer = null;
		shortBuffer = null;
		byteBuffer = null;
	}

	public void resize(int numBytes) {
		if(byteBuffer == null || numBytes > sizeInBytes) {
			sizeInBytes = numBytes;
			byteBuffer = BufferUtils.createByteBuffer(numBytes);
			shortBuffer = byteBuffer.asShortBuffer();
			intBuffer = byteBuffer.asIntBuffer();
			floatBuffer = byteBuffer.asFloatBuffer();
		}
	}

	public void setBytes(int numBytes, byte[] bytes) {
		resize(numBytes);
		byteBuffer.clear();
		byteBuffer.put(bytes, 0, numBytes);
		byteBuffer.flip();
	}

	public void setShorts(int numShorts, short[] shorts) {
		resize(numShorts * 2);
		shortBuffer.clear();
		shortBuffer.put(shorts, 0, numShorts);
		shortBuffer.flip();
	}

	public void setInts(int numInts, int[] ints) {
		resize(numInts * 4);
		intBuffer.clear();
		intBuffer.put(ints, 0, numInts);
		intBuffer.flip();
	}

	public void setFloats(int numFloats, float[] floats) {
		resize(numFloats * 4);
		floatBuffer.clear();
		floatBuffer.put(floats, 0, numFloats);
		floatBuffer.flip();
	}
}