package com.indiedev101;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL30;

public class Texture {
	public int id;
	public int target;
	public int width;
	public int height;
	public int format;

	public boolean isCreated() {
		return id > 0;
	}

	public void create(TextureDesc desc) {
		target = desc.target;
		width = desc.width;
		height = desc.height;

		id = GL11.glGenTextures();
		GL11.glBindTexture(target, id);
		GL11.glTexParameteri(target, GL11.GL_TEXTURE_MIN_FILTER, desc.minFilter);
		GL11.glTexParameteri(target, GL11.GL_TEXTURE_MAG_FILTER, desc.magFilter);
		GL11.glTexParameteri(target, GL11.GL_TEXTURE_WRAP_S, desc.wrapS);
		GL11.glTexParameteri(target, GL11.GL_TEXTURE_WRAP_T, desc.wrapT);

		if(desc.data != null)
			GL11.glTexImage2D(target, 0, desc.internalFormat, width, height, 0, desc.format, desc.type, desc.data);
		else if(desc.size > 0)
			GL11.glTexImage2D(target, 0, desc.internalFormat, width, height, 0, desc.format, desc.type, desc.size);

		if(desc.minFilter == GL11.GL_NEAREST_MIPMAP_NEAREST || desc.minFilter == GL11.GL_LINEAR_MIPMAP_NEAREST || desc.minFilter == GL11.GL_NEAREST_MIPMAP_LINEAR || desc.minFilter == GL11.GL_LINEAR_MIPMAP_LINEAR)
			GL30.glGenerateMipmap(target);

		format = desc.internalFormat;
	}

	public void destroy() {
		if(id > 0) {
			GL11.glDeleteTextures(id);
			id = 0;
		}
	}

	public void bind(int sampler) {
		GL13.glActiveTexture(GL13.GL_TEXTURE0 + sampler);
		GL11.glBindTexture(target, id);
	}

	public void unbind(int sampler) {
		GL13.glActiveTexture(GL13.GL_TEXTURE0 + sampler);
		GL11.glBindTexture(target, 0);
	}

	public static void unbind(int target, int sampler) {
		GL13.glActiveTexture(GL13.GL_TEXTURE0 + sampler);
		GL11.glBindTexture(target, 0);
	}

	public static Texture createTexture(Image image, TextureParams textureParams) {
		if(textureParams == null)
			textureParams = TextureParams.NEAREST_CLAMP;
		Texture texture = null;
		if(image != null) {
			TextureDesc textureDesc = new TextureDesc();
			textureDesc.width = image.width;
			textureDesc.height = image.height;
			textureDesc.magFilter = textureParams.magFilter;
			textureDesc.minFilter = textureParams.minFilter;
			textureDesc.wrapS = textureParams.wrapS;
			textureDesc.wrapT = textureParams.wrapT;
			if(image.bpp == 3)
				textureDesc.format = textureDesc.internalFormat = GL11.GL_RGB;
			else
				textureDesc.format = textureDesc.internalFormat = GL11.GL_RGBA;
			textureDesc.data = image.dataAsByteBuffer;

			texture = new Texture();
			texture.create(textureDesc);
		}
		return texture;
	}

	public static Texture createTexture(Image image) {
		return createTexture(image, null);
	}

	public static Texture createTexture(String name, TextureParams textureParams) {
		String path = IO.getFilePath(name);
		Image image = Image.createImage(path);
		Texture texture = createTexture(image, textureParams);
		return texture;
	}

	public static Texture createTexture(String name) {
		return createTexture(name, null);
	}

	public static Texture destroyTexture(Texture texture) {
		if(texture != null)
			texture.destroy();
		return null;
	}
}