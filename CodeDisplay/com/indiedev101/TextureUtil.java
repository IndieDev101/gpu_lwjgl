package com.indiedev101;

public class TextureUtil {
	public static int minX(int x, int y, int w, int h, byte[] textureData, int textureWidth) {
		int i;
		for(i = x; i < x + w; i++)
			for(int j = y; j < y + h; j++)
				if((textureData[(i * 4) + (j * textureWidth * 4) + 3] & 255) != 0)
					return i;
		return i;
	}

	public static int minY(int x, int y, int w, int h, byte[] textureData, int textureWidth) {
		int j;
		for(j = y; j < y + h; j++)
			for(int i = x; i < x + w; i++)
				if((textureData[(i * 4) + (j * textureWidth * 4) + 3] & 255) != 0)
					return j;
		return j;
	}

	public static int maxX(int x, int y, int w, int h, byte[] textureData, int textureWidth) {
		int i;
		for(i = x + w - 1; i >= x; i--)
			for(int j = y; j < y + h; j++)
				if((textureData[(i * 4) + (j * textureWidth * 4) + 3] & 255) != 0)
					return i;
		return i;
	}

	public static int maxY(int x, int y, int w, int h, byte[] textureData, int textureWidth) {
		int j;
		for(j = y + h - 1; j >= y; j--)
			for(int i = x; i < x + w; i++)
				if((textureData[(i * 4) + (j * textureWidth * 4) + 3] & 255) != 0)
					return j;
		return j;
	}
}