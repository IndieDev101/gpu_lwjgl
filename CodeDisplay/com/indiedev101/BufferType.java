package com.indiedev101;

import org.lwjgl.opengl.GL15;

public class BufferType {
	public static final int Vertex = GL15.GL_ARRAY_BUFFER;
	public static final int Index = GL15.GL_ELEMENT_ARRAY_BUFFER;
}