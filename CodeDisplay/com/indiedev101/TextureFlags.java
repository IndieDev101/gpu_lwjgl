package com.indiedev101;

public class TextureFlags {
	public static final int KeepData = 0x00000001;
	public static final int NoPreMul = 0x00000002;
	public static final int Indirect = 0x00000004;
	public static final int NEAREST_REPEAT = 0x000000010;
	public static final int NEAREST_CLAMP = 0x000000020;
	public static final int LINEAR_REPEAT = 0x000000040;
	public static final int LINEAR_CLAMP = 0x000000080;
	public static final int NEAREST_LINEAR_MIPMAP_LINEAR_REPEAT = 0x0000000100;
	public static final int NEAREST_LINEAR_MIPMAP_LINEAR_CLAMP = 0x0000000200;
	public static final int LINEAR_LINEAR_MIPMAP_LINEAR_REPEAT = 0x0000000400;
	public static final int LINEAR_LINEAR_MIPMAP_LINEAR_CLAMP = 0x0000000800;
	public static final int NEAREST_NEAREST_MIPMAP_LINEAR_REPEAT = 0x0000001000;
	public static final int NEAREST_NEAREST_MIPMAP_LINEAR_CLAMP = 0x00000002000;
}