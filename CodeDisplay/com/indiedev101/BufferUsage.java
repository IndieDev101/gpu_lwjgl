package com.indiedev101;

import org.lwjgl.opengl.GL15;

public class BufferUsage {
	public static final int Static = GL15.GL_STATIC_DRAW;
	public static final int Dynamic = GL15.GL_STREAM_DRAW;
}