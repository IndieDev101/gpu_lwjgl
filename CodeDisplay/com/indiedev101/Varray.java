package com.indiedev101;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

public class Varray {
	public int id;

	public boolean isCreated() {
		return id > 0;
	}

	public void create() {
		id = GL30.glGenVertexArrays();
	}

	public void destroy() {
		if(id > 0) {
			GL30.glDeleteVertexArrays(id);
			id = 0;
		}
	}

	public void bind() {
		GL30.glBindVertexArray(id);
	}

	public static void unbind() {
		GL30.glBindVertexArray(0);
	}

	public void addAttrib(int index, int size, int format, boolean normalized, int stride, long offset) {
		GL20.glEnableVertexAttribArray(index);
		GL20.glVertexAttribPointer(index, size, format, normalized, stride, offset);
	}

	public static Varray destroyVarray(Varray varray) {
		if(varray != null)
			varray.destroy();
		return null;
	}
}