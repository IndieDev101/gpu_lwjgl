package com.indiedev101;

public class BufferUtil {
	public static int Pos3Tex2(int i, float[] vertices, float x, float y, float z, float u, float v) {
		vertices[i++] = x;
		vertices[i++] = y;
		vertices[i++] = z;
		vertices[i++] = u;
		vertices[i++] = v;
		return i;
	}
}