package com.indiedev101;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

public class ShaderUtil {
	public static int createShader(int type, String source, String version, String... defines) {
		String string = "";

		string += "#version ";
		string += version != null ? version : "150";
		string += "\n";

		for(String define : defines) {
			string += "#define ";
			string += define;
			string += "\n";
		}

		string += source;

		int shader = GL20.glCreateShader(type);;
		GL20.glShaderSource(shader, string);
		GL20.glCompileShader(shader);

		int compileStatus = GL20.glGetShaderi(shader, GL20.GL_COMPILE_STATUS);
		if(compileStatus == GL11.GL_FALSE) {
			Log.debug("******************************\n");
			printShader(string);
			Log.debug("******************************\n");
			Log.error("%s\n", GL20.glGetShaderInfoLog(shader, 1000));
			GL20.glDeleteShader(shader);
			shader = 0;
		}

		return shader;
	}

	public static void printShader(String source) {
		int l = 0;
		int i = 0;
		while(i < source.length()) {
			int j = i;
			while(j < source.length()) {
				if(source.charAt(j) == 10) {
					break;
				} else {
					j++;
				}
			}
			if(j >= i && j < source.length()) {
				Log.debug("%d ", l);
				for(int k = i; k <= j; k++) {
					Log.debug("%c", source.charAt(k));
				}
				l++;
			}
			i = j + 1;
		}
	}
}