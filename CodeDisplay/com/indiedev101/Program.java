package com.indiedev101;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

public class Program {
	public int id;
	public HashMap<String, Uniform> uniforms;
	public String name;
	public ArrayList<Shader> shaders;
	public ArrayList<Vattrib> vattribs;
	public ArrayList<Sampler> samplers;

	public boolean isCreated() {
		return id > 0;
	}

	public Uniform getUniform(String name, boolean warnIfNotExists) {
		if(id <= 0)
			return null;
		if(uniforms == null)
			uniforms = new HashMap<String, Uniform>();
		Uniform uniform = uniforms.get(name);
		if(uniform == null) {
			int location = GL20.glGetUniformLocation(id, name);
			if(location >= 0) {
				uniform = new Uniform(location);
				uniforms.put(name, uniform);
			} else if(warnIfNotExists) {
				if(this.name != null)
					Log.debug("Failed to find uniform %s in program %s\n", name, this.name);
				else
					Log.debug("Failed to find uniform %s\n", name);
			}
		}
		return uniform;
	}

	public Uniform getUniform(String name) {
		return getUniform(name, false);
	}

	public boolean create() {
		boolean success = true;

		int id = GL20.glCreateProgram();

		if(shaders != null) {
			for(int i = 0; i < shaders.size(); i++) {
				Shader shader = shaders.get(i);
				if(shader.id > 0)
					GL20.glAttachShader(id, shader.id);
			}
		}

		if(vattribs != null) {
			for(int i = 0; i < vattribs.size(); i++) {
				Vattrib vattrib = vattribs.get(i);
				GL20.glBindAttribLocation(id, vattrib.index, vattrib.name);
			}
		}

		GL20.glLinkProgram(id);
		int linkStatus = GL20.glGetProgrami(id, GL20.GL_LINK_STATUS);
		if(linkStatus == GL11.GL_FALSE) {
			String infoLog = GL20.glGetProgramInfoLog(id, 1000);
			if(name != null)
				Log.error("%s: %s\n", name, infoLog);
			else
				Log.error("%s\n", infoLog);
			success = false;
		}

		if(shaders != null) {
			for(int i = 0; i < shaders.size(); i++) {
				Shader shader = shaders.get(i);
				if(shader.id > 0)
					GL20.glDetachShader(id, shader.id);
			}
		}

		if(success && samplers != null) {
			GL20.glUseProgram(id);

			for(int i = 0; i < samplers.size(); i++) {
				Sampler sampler = samplers.get(i);
				int location = GL20.glGetUniformLocation(id, sampler.name);
				if(location != -1)
					GL20.glUniform1i(location, sampler.index);
			}

			GL20.glUseProgram(0);
		}

		if(success && uniforms != null) {
			for(Map.Entry<String, Uniform> i : uniforms.entrySet()) {
				String uniformName = i.getKey();
				Uniform location = i.getValue();
				location.location = GL20.glGetUniformLocation(id, uniformName);
			}
		}

		if(success) {
			if(this.id > 0)
				GL20.glDeleteProgram(this.id);
			this.id = id;
		} else {
			GL20.glDeleteProgram(id);
		}

		return success;
	}

	public void destroy() {
		if(uniforms != null)
			uniforms.clear();

		if(id > 0) {
			GL20.glDeleteProgram(id);
			id = 0;
		}
	}

	public void bind() {
		GL20.glUseProgram(id);
	}

	public static void unbind() {
		GL20.glUseProgram(0);
	}

	public void validate() {
		if(!Sys.debug)
			return;
		GL20.glValidateProgram(id);
		int validateStatus = GL20.glGetProgrami(id, GL20.GL_VALIDATE_STATUS);
		if(validateStatus == GL11.GL_FALSE) {
			String infoLog = GL20.glGetProgramInfoLog(id, 1000);
			if(name != null)
				Log.error("%s: %s\n", name, infoLog);
			else
				Log.error("%s\n", infoLog);
		}
	}

	public void setName(String name) {
		this.name = name;
	}

	public void addShader(Shader shader) {
		if(shaders == null)
			shaders = new ArrayList<Shader>();
		shaders.add(shader);
	}

	public void addVattrib(int index, String name) {
		if(vattribs == null)
			vattribs = new ArrayList<Vattrib>();
		vattribs.add(new Vattrib(name, index));
	}

	public void addSampler(int index, String name) {
		if(samplers == null)
			samplers = new ArrayList<Sampler>();
		samplers.add(new Sampler(name, index));
	}

	public void setFloat(Uniform uniform, float value) {
		if(uniform != null)
			GL20.glUniform1f(uniform.location, value);
	}

	public void setVector(Uniform uniform, Vec3f value) {
		if(uniform != null)
			GL20.glUniform3f(uniform.location, value.x, value.y, value.z);
	}

	public void setMatrix(Uniform uniform, FloatBuffer value) {
		if(uniform != null)
			GL20.glUniformMatrix4fv(uniform.location, false, value);
	}

	public static Program destroyProgram(Program program) {
		if(program != null)
			program.destroy();
		return null;
	}
}