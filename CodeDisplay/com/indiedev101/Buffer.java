package com.indiedev101;

import org.lwjgl.opengl.GL15;

public class Buffer {
	public int id;
	public int type;
	public int usage;
	public int size;

	public boolean isCreated() {
		return id > 0;
	}

	public void create(BufferDesc desc) {
		type = desc.type;
		usage = desc.usage;

		id = GL15.glGenBuffers();
		GL15.glBindBuffer(type, id);

		if(desc.dataAsByteBuffer != null)
			GL15.glBufferData(type, desc.dataAsByteBuffer, usage);
		else if(desc.dataAsShortBuffer != null)
			GL15.glBufferData(type, desc.dataAsShortBuffer, usage);
		else if(desc.dataAsIntBuffer != null)
			GL15.glBufferData(type, desc.dataAsIntBuffer, usage);
		else if(desc.dataAsFloatBuffer != null)
			GL15.glBufferData(type, desc.dataAsFloatBuffer, usage);
		else if(desc.size > 0)
			GL15.glBufferData(type, desc.size, usage);

		size = desc.size;
	}

	public void destroy() {
		if(id > 0) {
			GL15.glDeleteBuffers(id);
			id = 0;
		}
	}

	public void bind() {
		GL15.glBindBuffer(type, id);
	}

	public void unbind() {
		GL15.glBindBuffer(type, 0);
	}

	public static void unbind(int type) {
		GL15.glBindBuffer(type, 0);
	}
	
	public static Buffer destroyBuffer(Buffer buffer) {
		if(buffer != null)
			buffer.destroy();
		return null;
	}
}