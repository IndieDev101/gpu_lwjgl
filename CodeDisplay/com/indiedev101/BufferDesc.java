package com.indiedev101;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

import org.lwjgl.BufferUtils;

public class BufferDesc {
	public int type;
	public int usage;
	public int size;
	public ByteBuffer dataAsByteBuffer;
	public ShortBuffer dataAsShortBuffer;
	public IntBuffer dataAsIntBuffer;
	public FloatBuffer dataAsFloatBuffer;

	public void setType(int type) {
		this.type = type;
	}

	public void setUsage(int usage) {
		this.usage = usage;
	}

	public void setSize(int sizeInBytes) {
		size = sizeInBytes;
	}

	public void setData(int sizeInBytes, byte[] data) {
		dataAsByteBuffer = BufferUtils.createByteBuffer(sizeInBytes);
		dataAsByteBuffer.put(data, 0, sizeInBytes);
		dataAsByteBuffer.flip();
		dataAsByteBuffer.limit(sizeInBytes);
	}

	public void setData(int sizeInShorts, short[] data) {
		dataAsShortBuffer = BufferUtils.createShortBuffer(sizeInShorts);
		dataAsShortBuffer.put(data, 0, sizeInShorts);
		dataAsShortBuffer.flip();
		dataAsShortBuffer.limit(sizeInShorts);
	}

	public void setData(int sizeInInts, int[] data) {
		dataAsIntBuffer = BufferUtils.createIntBuffer(sizeInInts);
		dataAsIntBuffer.put(data, 0, sizeInInts);
		dataAsIntBuffer.flip();
		dataAsIntBuffer.limit(sizeInInts);
	}

	public void setData(int sizeInFloats, float[] data) {
		dataAsFloatBuffer = BufferUtils.createFloatBuffer(sizeInFloats);
		dataAsFloatBuffer.put(data, 0, sizeInFloats);
		dataAsFloatBuffer.flip();
		dataAsFloatBuffer.limit(sizeInFloats);
	}
}