package com.indiedev101;

public class BlendMode {
	public static final int Default = 0;
	public static final int Solid = 1;
	public static final int SolidAlphaTest = 2;
	public static final int Alpha = 3;
	public static final int AlphaPreMultiply = 4;
	public static final int Additive = 5;
	public static final int AlphaAdditive = 6;
	public static final int Multiply = 7;
	public static final int Screen = 8;
	public static final int Subtractive = 9;
	public static final int AlphaSubtractive = 10;
	public static final int Crosshair = 11;
	public static final int Count = 12;
	public static final int Unknown = 13;
}