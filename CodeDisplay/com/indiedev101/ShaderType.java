package com.indiedev101;

import org.lwjgl.opengl.GL20;

public class ShaderType {
	public static final int Vert = GL20.GL_VERTEX_SHADER;
	public static final int Frag = GL20.GL_FRAGMENT_SHADER;
}