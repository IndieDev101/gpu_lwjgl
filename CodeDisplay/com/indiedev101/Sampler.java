package com.indiedev101;

public class Sampler {
	public String name;
	public int index;

	public Sampler(String name, int index) {
		this.name = name;
		this.index = index;
	}
}