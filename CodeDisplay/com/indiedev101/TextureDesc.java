package com.indiedev101;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

public class TextureDesc {
	public int target = GL11.GL_TEXTURE_2D;
	public int width;
	public int height;
	public int minFilter = GL11.GL_NEAREST;
	public int magFilter = GL11.GL_NEAREST;
	public int wrapS = GL12.GL_CLAMP_TO_EDGE;
	public int wrapT = GL12.GL_CLAMP_TO_EDGE;
	public int internalFormat = GL11.GL_RGBA;
	public int format = GL11.GL_RGBA;
	public int type = GL11.GL_UNSIGNED_BYTE;
	public int size;
	public ByteBuffer data;
	
	public void setData(int sizeInInts, int[] data) {
		this.data = BufferUtils.createByteBuffer(sizeInInts * 4);
		IntBuffer buff = this.data.asIntBuffer();
		buff.put(data, 0, sizeInInts);
		buff.flip();
		buff.limit(sizeInInts);
	}
}