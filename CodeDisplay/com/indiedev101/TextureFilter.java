package com.indiedev101;

import org.lwjgl.opengl.GL11;

public final class TextureFilter {
	public static final int NEAREST = GL11.GL_NEAREST;
	public static final int LINEAR = GL11.GL_LINEAR;
	public static final int LINEAR_MIPMAP_LINEAR = GL11.GL_LINEAR_MIPMAP_LINEAR;
	public static final int NEAREST_MIPMAP_LINEAR = GL11.GL_NEAREST_MIPMAP_LINEAR;
}