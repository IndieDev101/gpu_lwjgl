package com.indiedev101;

public class TextureParams {
	public static final TextureParams NEAREST_REPEAT = new TextureParams(TextureFilter.NEAREST, TextureFilter.NEAREST, TextureWrap.REPEAT, TextureWrap.REPEAT);
	public static final TextureParams NEAREST_CLAMP = new TextureParams(TextureFilter.NEAREST, TextureFilter.NEAREST, TextureWrap.CLAMP, TextureWrap.CLAMP);

	public static final TextureParams LINEAR_REPEAT = new TextureParams(TextureFilter.LINEAR, TextureFilter.LINEAR, TextureWrap.REPEAT, TextureWrap.REPEAT);
	public static final TextureParams LINEAR_CLAMP = new TextureParams(TextureFilter.LINEAR, TextureFilter.LINEAR, TextureWrap.CLAMP, TextureWrap.CLAMP);

	public static final TextureParams NEAREST_LINEAR_MIPMAP_LINEAR_REPEAT = new TextureParams(TextureFilter.NEAREST, TextureFilter.LINEAR_MIPMAP_LINEAR, TextureWrap.REPEAT, TextureWrap.REPEAT);
	public static final TextureParams NEAREST_LINEAR_MIPMAP_LINEAR_CLAMP = new TextureParams(TextureFilter.NEAREST, TextureFilter.LINEAR_MIPMAP_LINEAR, TextureWrap.CLAMP, TextureWrap.CLAMP);

	public static final TextureParams LINEAR_LINEAR_MIPMAP_LINEAR_REPEAT = new TextureParams(TextureFilter.LINEAR, TextureFilter.LINEAR_MIPMAP_LINEAR, TextureWrap.REPEAT, TextureWrap.REPEAT);
	public static final TextureParams LINEAR_LINEAR_MIPMAP_LINEAR_CLAMP = new TextureParams(TextureFilter.LINEAR, TextureFilter.LINEAR_MIPMAP_LINEAR, TextureWrap.CLAMP, TextureWrap.CLAMP);

	public static final TextureParams NEAREST_NEAREST_MIPMAP_LINEAR_REPEAT = new TextureParams(TextureFilter.NEAREST, TextureFilter.NEAREST_MIPMAP_LINEAR, TextureWrap.REPEAT, TextureWrap.REPEAT);
	public static final TextureParams NEAREST_NEAREST_MIPMAP_LINEAR_CLAMP = new TextureParams(TextureFilter.NEAREST, TextureFilter.NEAREST_MIPMAP_LINEAR, TextureWrap.CLAMP, TextureWrap.CLAMP);

	public final int magFilter;
	public final int minFilter;
	public final int wrapS;
	public final int wrapT;
	public final String hash;

	public TextureParams(int magFilter, int minFilter, int wrapS, int wrapT) {
		this.magFilter = magFilter;
		this.minFilter = minFilter;
		this.wrapS = wrapS;
		this.wrapT = wrapT;
		hash = getFilter(magFilter) + getFilter(minFilter) + getWrap(wrapS) + getWrap(wrapT);
	}

	public final boolean hasMipmap() {
		return minFilter != TextureFilter.NEAREST && minFilter != TextureFilter.LINEAR;
	}

	private static final String getFilter(int filter) {
		switch(filter) {
			case TextureFilter.NEAREST:
				return "1";
			case TextureFilter.LINEAR:
				return "2";
			case TextureFilter.LINEAR_MIPMAP_LINEAR:
				return "3";
			case TextureFilter.NEAREST_MIPMAP_LINEAR:
				return "4";
			default:
				return "0";
		}
	}

	private static final String getWrap(int wrap) {
		switch(wrap) {
			case TextureWrap.REPEAT:
				return "1";
			case TextureWrap.CLAMP:
				return "2";
			default:
				return "0";
		}
	}
}