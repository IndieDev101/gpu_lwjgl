package com.indiedev101;

import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL14;

public class GPU {
	public static int width;
	public static int height;
	public static float scale = 1;

	public static int viewX;
	public static int viewY;
	public static int viewW;
	public static int viewH;

	public static int safeX1;
	public static int safeY1;
	public static int safeX2;
	public static int safeY2;

	public static void resize() {
		IntBuffer w = BufferUtils.createIntBuffer(1);
		IntBuffer h = BufferUtils.createIntBuffer(1);
		GLFW.glfwGetFramebufferSize(App.window, w, h);
		width = w.get();
		height = h.get();
		int size = width < height ? width : height;
		scale = (int)(size / 480.0f);
		if(scale < 1)
			scale = 1;
	}

	public static void create() {
		createIndexBuffer();
		createWhiteTexture();
	}

	public static void destroy() {
		destroyWhiteTexture();
		destroyIndexBuffer();
	}

	public static void updateScale() {
		int size = width < height ? width : height;
		scale = (int)(size / 480.0f);
		if(scale < 1)
			scale = 1;
	}

	public static int getWidth() {
		return width;
	}

	public static int getHeight() {
		return height;
	}

	public static float getAspectRatio() {
		return (float)width / (float)height;
	}

	public static boolean isPortrait() {
		return width <= height;
	}

	public static boolean isLandscape() {
		return width >= height;
	}

	public static Texture whiteTexture;

	private static void createWhiteTexture() {
		int[] textureData = new int[1];
		textureData[0] = 0xffffffff;

		TextureDesc textureDesc = new TextureDesc();
		textureDesc.width = 1;
		textureDesc.height = 1;
		textureDesc.format = textureDesc.internalFormat = GL11.GL_RGBA;
		textureDesc.setData(1, textureData);

		whiteTexture = new Texture();
		whiteTexture.create(textureDesc);
	}

	private static void destroyWhiteTexture() {
		if(whiteTexture != null) {
			whiteTexture.destroy();
			whiteTexture = null;
		}
	}

	public static Buffer indexBuffer;

	private static void createIndexBuffer() {
		int numIndices = (65536 / 4) * 6;
		short[] indices = new short[numIndices];
		for(int idx = 0, vtx = 4; vtx <= 65536; vtx += 4) {
			short v4 = (short)(vtx - 4);
			short v3 = (short)(vtx - 3);
			short v2 = (short)(vtx - 2);
			short v1 = (short)(vtx - 1);
			indices[idx++] = v4;
			indices[idx++] = v3;
			indices[idx++] = v2;
			indices[idx++] = v4;
			indices[idx++] = v2;
			indices[idx++] = v1;
		}

		BufferDesc indexBufferDesc = new BufferDesc();
		indexBufferDesc.setType(BufferType.Index);
		indexBufferDesc.setUsage(BufferUsage.Static);
		indexBufferDesc.setData(numIndices, indices);

		indexBuffer = new Buffer();
		indexBuffer.create(indexBufferDesc);
	}

	private static void destroyIndexBuffer() {
		if(indexBuffer != null) {
			indexBuffer.destroy();
			indexBuffer = null;
		}
	}

	public static float lineWidth = -1;
	public static float pointSize = -1;
	public static int wireframe = -1;
	public static int culling = -1;
	public static int depthTest = -1;
	public static int depthMask = -1;
	public static int blendMode = -1;

	public static void setLineWidth(float width) {
		//if(lineWidth != width) {
		GL11.glLineWidth(width);
		lineWidth = width;
		//}
	}

	public static void setPointSize(float size) {
		//if(pointSize != size) {
		GL11.glPointSize(size);
		pointSize = size;
		//}
	}

	public static void setWireframe(boolean enable) {
		if(enable) {
			//if(wireframe != 1) {
			GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_LINE);
			wireframe = 1;
			//}
		} else {
			//if(wireframe != 0) {
			GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_FILL);
			wireframe = 0;
			//}
		}
	}

	public static void setCulling(boolean enable) {
		if(enable) {
			//if(culling != 1) {
			GL11.glEnable(GL11.GL_CULL_FACE);
			culling = 1;
			//}
		} else {
			//if(culling != 0) {
			GL11.glDisable(GL11.GL_CULL_FACE);
			culling = 0;
			//}
		}
	}

	public static void setDepthTest(boolean enable) {
		if(enable) {
			//if(depthTest != 1) {
			GL11.glEnable(GL11.GL_DEPTH_TEST);
			depthTest = 1;
			//}
		} else {
			//if(depthTest != 0) {
			GL11.glDisable(GL11.GL_DEPTH_TEST);
			depthTest = 0;
			//}
		}
	}

	public static void setDepthMask(boolean enable) {
		if(enable) {
			//if(depthMask != 1) {
			GL11.glDepthMask(true);
			depthMask = 1;
			//}
		} else {
			//if(depthMask != 0) {
			GL11.glDepthMask(false);
			depthMask = 0;
			//}
		}
	}

	public static void setBlendMode(int blendMode) {
		//if(GPU.blendMode == blendMode)
		//return;

		boolean useBlend = true;
		int blendSrc = GL11.GL_ONE;
		int blendDst = GL11.GL_ONE_MINUS_SRC_ALPHA;

		boolean useAlpha = true;
		int alphaFnc = GL11.GL_GREATER;
		float alphaRef = 0;

		boolean subtractive = false;

		switch(blendMode) {
			case BlendMode.Default:
			case BlendMode.Solid:
				useBlend = false;
				useAlpha = false;
				break;
			case BlendMode.SolidAlphaTest:
				useBlend = false;
				break;
			case BlendMode.Alpha:
				blendSrc = GL11.GL_SRC_ALPHA;
				break;
			case BlendMode.AlphaPreMultiply:
				break;
			case BlendMode.Additive:
				blendDst = GL11.GL_ONE;
				break;
			case BlendMode.AlphaAdditive:
				blendSrc = GL11.GL_SRC_ALPHA;
				blendDst = GL11.GL_ONE;
				break;
			case BlendMode.Multiply:
				blendSrc = GL11.GL_ZERO;
				blendDst = GL11.GL_SRC_COLOR;
				break;
			case BlendMode.Screen:
				blendSrc = GL11.GL_DST_COLOR;
				blendDst = GL11.GL_ONE_MINUS_SRC_ALPHA;
				break;
			case BlendMode.Subtractive:
				blendSrc = GL11.GL_ONE;
				blendDst = GL11.GL_ONE;
				subtractive = true;
				break;
			case BlendMode.AlphaSubtractive:
				blendSrc = GL11.GL_SRC_ALPHA;
				blendDst = GL11.GL_ONE;
				subtractive = true;
				break;
			case BlendMode.Crosshair:
				blendSrc = GL11.GL_ONE_MINUS_DST_COLOR;
				blendDst = GL11.GL_ZERO;
				break;
		}

		if(subtractive)
			GL14.glBlendEquation(GL14.GL_FUNC_REVERSE_SUBTRACT);
		else
			GL14.glBlendEquation(GL14.GL_FUNC_ADD);

		if(useBlend) {
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glBlendFunc(blendSrc, blendDst);
		} else {
			GL11.glDisable(GL11.GL_BLEND);
		}

		/*if(useAlpha) {
			GL11.glEnable(GL11.GL_ALPHA_TEST);
			GL11.glAlphaFunc(alphaFnc, alphaRef);
		} else {
			GL11.glDisable(GL11.GL_ALPHA_TEST);
		}*/

		GPU.blendMode = blendMode;
	}

	public static void setViewport(int x, int y, int w, int h) {
		GL11.glViewport(x, y, w, h);
		viewX = x;
		viewY = y;
		viewW = width;
		viewH = height;
		safeX1 = (int)((float)width * 0.1f);
		safeY1 = (int)((float)height * 0.1f);
		safeX2 = (int)((float)width * 0.9f);
		safeY2 = (int)((float)height * 0.9f);
	}

	public static void setClearColor(float r, float g, float b, float a) {
		GL11.glClearColor(r, g, b, a);
	}

	public static void setClearColor(int c) {
		int r = (c & 0x00ff0000) >> 16;
		int g = (c & 0x0000ff00) >> 8;
		int b = (c & 0x000000ff);
		int a = (c & 0xff000000) >> 24;
		GL11.glClearColor((float)r / 255.0f, (float)g / 255.0f, (float)b / 255.0f, (float)a / 255.0f);
	}

	public static void clear(boolean color, boolean depth, boolean stencil) {
		int mask = 0;
		if(color)
			mask |= GL11.GL_COLOR_BUFFER_BIT;
		if(depth)
			mask |= GL11.GL_DEPTH_BUFFER_BIT;
		if(stencil)
			mask |= GL11.GL_STENCIL_BUFFER_BIT;
		GL11.glClear(mask);
	}

	public static void setScissoring(boolean enable) {
		if(enable)
			GL11.glEnable(GL11.GL_SCISSOR_TEST);
		else
			GL11.glDisable(GL11.GL_SCISSOR_TEST);
	}

	public static void setScissorRect(int x, int y, int w, int h) {
		GL11.glScissor(x, y, w, h);
	}
}