package com.indiedev101;

import org.lwjgl.opengl.GL11;

public final class PrimType {
	public static final int Points = GL11.GL_POINTS;
	public static final int Lines = GL11.GL_LINES;
	public static final int LineStrip = GL11.GL_LINE_STRIP;
	public static final int Triangles = GL11.GL_TRIANGLES;
	public static final int TriangleStrip = GL11.GL_TRIANGLE_STRIP;
	public static final int TriangleFan = GL11.GL_TRIANGLE_FAN;
	public static final int Quads = GL11.GL_QUADS;
}