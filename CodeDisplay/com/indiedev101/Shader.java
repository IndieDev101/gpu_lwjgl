package com.indiedev101;

import java.util.ArrayList;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

public class Shader {
	public int id;
	public int type;
	public String name;
	public String path;
	public String source;
	public String version;
	public ArrayList<Define> defines;

	public boolean isCreated() {
		return id > 0;
	}

	public boolean create() {
		boolean success = true;

		if(source == null) {
			source = "";

			source += "#version ";
			if(version != null)
				source += version;
			else
				source += "150";
			source += "\n";

			if(defines != null) {
				for(int i = 0; i < defines.size(); i++) {
					Define define = defines.get(i);
					source += "#define ";
					source += define.name;
					if(define.value != null) {
						source += " ";
						source += define.value;
					}
					source += "\n";
				}
			}

			if(path != null) {
				String string = IO.getFileAsString(path);
				if(string != null)
					source += string;
				else
					Log.error("Failed to load %s\n", path);
			}
		}

		int id = GL20.glCreateShader(type);
		GL20.glShaderSource(id, source);
		GL20.glCompileShader(id);

		int compileStatus = GL20.glGetShaderi(id, GL20.GL_COMPILE_STATUS);
		if(compileStatus == GL11.GL_FALSE) {
			ShaderUtil.printShader(source);
			String infoLog = GL20.glGetShaderInfoLog(id, 1000);
			if(name != null)
				Log.error("%s: %s\n", name, infoLog);
			else
				Log.error("%s\n", infoLog);
			success = false;
		}

		if(success) {
			if(this.id > 0)
				GL20.glDeleteShader(this.id);
			this.id = id;
		} else {
			GL20.glDeleteShader(id);
		}

		return success;
	}

	public void destroy() {
		if(id > 0) {
			GL20.glDeleteShader(id);
			id = 0;
		}
	}

	public void setType(int type) {
		this.type = type;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPath(String path) {
		this.path = IO.getFilePath(path);
	}

	public void setSource(String source) {
		this.source = source;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public void addDefine(String name, String value) {
		if(defines == null)
			defines = new ArrayList<Define>();
		defines.add(new Define(name, value));
	}

	public void addDefine(String name) {
		addDefine(name, null);
	}
}