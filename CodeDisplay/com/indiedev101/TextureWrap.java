package com.indiedev101;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

public class TextureWrap {
	public static final int REPEAT = GL11.GL_REPEAT;
	public static final int CLAMP = GL12.GL_CLAMP_TO_EDGE;
}