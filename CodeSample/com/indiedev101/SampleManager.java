package com.indiedev101;

import java.util.ArrayList;

public abstract class SampleManager {
	private int active;
	private ArrayList<SampleWidget> widgets = new ArrayList<SampleWidget>();

	protected void addSample(String longName) {
		widgets.add(new SampleWidget(longName));
	}

	protected abstract String addSamples();

	public void create() {
		String sample = addSamples();
		if(sample != null)
			createSample(sample);
	}

	public void destroy() {
		destroySample();
		widgets.clear();
	}

	public void resize(int width, int height) {
		if(Sample.instance != null)
			Sample.instance.resize(width, height);
	}

	public void variableUpdate(float timeStep) {
		if(Sample.instance != null) {
			boolean restart = false;
			if(Keyboard.isKeyPress(Keyboard.KeyR))
				restart = true;
			if(restart) {
				destroySample();
				createSample();
			} else {
				boolean quit = false;
				if(Keyboard.isKeyPress(Keyboard.KeyLeft))
					quit = true;
				if(quit) {
					destroySample();
				} else {
					Sample.instance.variableUpdate(timeStep);
				}
			}
		} else {
			boolean enterSample = false;

			if(Mouse.isButtonDown(0)) {
				for(int i = 0; i < widgets.size(); i++) {
					SampleWidget widget = widgets.get(i);

					if(widget.inside(Mouse.currX, Mouse.currY)) {
						enterSample = true;
						active = i;
						break;
					}
				}
			}

			if(Keyboard.isKeyPress(Keyboard.KeyRight)) {
				enterSample = true;
			}

			if(enterSample) {
				createSample();
			} else {
				if(Keyboard.isKeyPress(Keyboard.KeyUp)) {
					if(--active < 0)
						active = 0;
				}

				if(Keyboard.isKeyPress(Keyboard.KeyDown)) {
					if(++active >= widgets.size())
						active = widgets.size() - 1;
				}
			}
		}
	}

	public void fixedUpdate(float timeStep) {
		if(Sample.instance != null)
			Sample.instance.fixedUpdate(timeStep);
	}

	public void render() {
		if(Sample.instance != null) {
			Sample.instance.render();
		} else {
			View view = View.instance;
			Font font = Font.instance;

			GPU.setBlendMode(BlendMode.Alpha);
			GPU.setDepthMask(false);
			GPU.setDepthTest(false);

			GPU.setClearColor(0xff000000);
			GPU.clear(true, false, false);

			view.set2D(GPU.width, GPU.height, 0, 1);
			font.bind();

			float x = 20;
			float y = GPU.height - 15;
			float s = 2;
			float maxW = 0;

			for(int i = 0; i < widgets.size(); i++) {
				SampleWidget widget = widgets.get(i);
				String t = String.format("- %s", widget.shortName);

				int c = i == active ? 0xff00bb00 : 0xffbbbbbb;
				float w = font.getTextWidth(t, s);
				float h = font.getTextHeight(s);

				widget.x1 = x;
				widget.y2 = y;
				widget.x2 = x + w;
				widget.y1 = y - h;

				font.addText(t, x, y, s, c, Align.None, Align.Top);
				y -= h * 1.5f;

				if(w > maxW)
					maxW = w;

				if(y <= 15) {
					y = GPU.height - 15;
					x += maxW * 1.5f;
					maxW = 0;
				}
			}

			font.unbind();
		}
	}

	public boolean mouseDown(int button, int x, int y) {
		if(Sample.instance != null)
			return Sample.instance.mouseDown(button, x, y);
		else
			return false;
	}

	public void mouseMove(int mx, int my, int px, int py) {
		if(Sample.instance != null)
			Sample.instance.mouseMove(mx, my, px, py);
	}

	public void mouseUp(int button, int x, int y) {
		if(Sample.instance != null)
			Sample.instance.mouseUp(button, x, y);
	}

	public void keyDown(int key) {
		if(Sample.instance != null)
			Sample.instance.keyDown(key);
	}

	public void keyUp(int key) {
		if(Sample.instance != null)
			Sample.instance.keyUp(key);
	}

	private void destroySample() {
		if(Sample.instance != null) {
			Sample.instance.destroy();
			Sample.instance = null;
		}
	}

	private void createSample() {
		if(active < 0 || active >= widgets.size())
			return;

		SampleWidget widget = widgets.get(active);
		Log.debug("Entering: %s\n", widget.longName);
		Sample.instance = widget.newSample();

		Sample.instance.shortName = widget.shortName;
		Sample.instance.longName = widget.longName;
		Sample.instance.create();
		Sample.instance.variableUpdate(0);
		Sample.instance.fixedUpdate(0);
	}

	private void createSample(String name) {
		for(int i = 0; i < widgets.size(); i++) {
			SampleWidget widget = widgets.get(i);
			if(name == widget.longName) {
				active = i;
				createSample();
				return;
			}
		}
	}

	public static SampleManager instance;

	public static void createInstance() {
		if(instance == null) {
			instance = new SampleLauncher();
			instance.create();
		}
	}

	public static void destroyInstance() {
		if(instance != null) {
			instance.destroy();
			instance = null;
		}
	}

	public static void resizeInstance(int width, int height) {
		if(instance != null)
			instance.resize(width, height);
	}

	public static void variableUpdateInstance(float timeStep) {
		if(instance != null)
			instance.variableUpdate(timeStep);
	}

	public static void fixedUpdateInstance(float timeStep) {
		if(instance != null)
			instance.fixedUpdate(timeStep);
	}

	public static void renderInstance() {
		if(instance != null)
			instance.render();
	}

	public static boolean mouseDownInstance(int button, int x, int y) {
		if(instance != null)
			return instance.mouseDown(button, x, y);
		else
			return false;
	}

	public static void mouseMoveInstance(int mx, int my, int px, int py) {
		if(instance != null)
			instance.mouseMove(mx, my, px, py);
	}

	public static void mouseUpInstance(int button, int x, int y) {
		if(instance != null)
			instance.mouseUp(button, x, y);
	}

	public static void keyDownInstance(int key) {
		if(instance != null)
			instance.keyDown(key);
	}

	public static void keyUpInstance(int key) {
		if(instance != null)
			instance.keyUp(key);
	}
}