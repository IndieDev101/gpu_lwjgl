package com.indiedev101;

public abstract class Sample {
	public String shortName;
	public String longName;

	public void create() {
		onCreate();
	}

	public void destroy() {
		onDestroy();
	}

	public void resize(int width, int height) {
		onResize(width, height);
	}

	public void variableUpdate(float timeStep) {
		onVariableUpdate(timeStep);
	}

	public void fixedUpdate(float timeStep) {
		onFixedUpdate(timeStep);
	}

	public void render() {
		onRender();
	}

	public boolean mouseDown(int button, int x, int y) {
		return onMouseDown(button, x, y);
	}

	public void mouseMove(int mx, int my, int px, int py) {
		onMouseMove(mx, my, px, py);
	}

	public void mouseUp(int button, int x, int y) {
		onMouseUp(button, x, y);
	}

	public void keyDown(int key) {
		onKeyDown(key);
	}

	public void keyUp(int key) {
		onKeyUp(key);
	}

	public void onCreate() {
	}

	public void onDestroy() {
	}

	public void onResize(int width, int height) {
	}

	public void onVariableUpdate(float timeStep) {
	}

	public void onFixedUpdate(float timeStep) {
	}

	public void onRender() {
	}

	public boolean onMouseDown(int button, int x, int y) {
		return false;
	}

	public void onMouseMove(int mx, int my, int px, int py) {
	}

	public void onMouseUp(int button, int x, int y) {
	}

	public void onKeyDown(int key) {
	}

	public void onKeyUp(int key) {
	}

	public static Sample instance;
}