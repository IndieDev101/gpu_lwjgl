package com.indiedev101;

public abstract class Sample3D extends Sample {
	public int clearColor = 0xff6495ed;
	public boolean paused;

	@Override
	public void create() {
		FPS.createInstance();
		Camera.createInstance();

		onCreate();
	}

	@Override
	public void destroy() {
		onDestroy();

		Camera.destroyInstance();
		FPS.destroyInstance();
	}

	@Override
	public void variableUpdate(float timeStep) {
		if(Keyboard.isKeyPress(Keyboard.KeyP))
			paused = !paused;

		FPS.updateInstance(timeStep);

		if(!paused) {
			Camera.updateInstance(timeStep);
			onVariableUpdate(timeStep);
		}
	}

	@Override
	public void render() {
		Camera camera = Camera.instance;
		View view = View.instance;

		view.set3D(camera.fieldOfView, camera.aspectRatio, camera.nearClip, camera.farClip, camera.pos, camera.dir, camera.up, camera.viewMatrix, camera.projMatrix);
		view.set2D(GPU.width, GPU.height, 0, 1);

		GPU.setBlendMode(BlendMode.Solid);
		GPU.setDepthTest(true);
		GPU.setDepthMask(true);

		GPU.setClearColor(clearColor);
		GPU.clear(true, true, true);

		onRender3D();

		GPU.setBlendMode(BlendMode.Alpha);
		GPU.setDepthMask(false);
		GPU.setDepthTest(false);

		onRender2D();
	}

	public void renderInfo() {
		Font font = Font.instance;
		font.bind();
		FPS.renderInstance(font, shortName);
		font.unbind();
		FPS.renderInstance();
	}

	public void onRender3D() {
	}

	public void onRender2D() {
	}
}