package com.indiedev101;

public class SampleWidget {
	public String shortName;
	public String longName;
	public float x1;
	public float y1;
	public float x2;
	public float y2;

	public SampleWidget(String name) {
		longName = name;
		if(name.startsWith("Sample"))
			shortName = name.substring(6);
		else
			shortName = name;
	}

	public boolean inside(float x, float y) {
		return x >= x1 && y >= y1 && x <= x2 && y <= y2;
	}

	public Sample newSample() {
		return (Sample)Sys.classForName(longName);
	}
}