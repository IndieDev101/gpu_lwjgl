package com.indiedev101;

import java.nio.FloatBuffer;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

public class Grid {
	private int vertexCount;
	private Varray vertexArray;
	private Buffer vertexBuffer;
	private Program program;
	private Uniform uniformViewMatrix;
	private Uniform uniformProjMatrix;

	public void create() {
		int blocks = 16;
		int chunks = 3;
		int count = blocks * chunks;
		float scale = 1;
		float y = 0;

		int blockVertexCount = ((count * 2) + 1) * 2;
		int chunkVertexCount = blockVertexCount * 2;
		int xaxisVertexCount = 2;
		int yaxisVertexCount = 2;
		int zaxisVertexCount = 2;
		vertexCount = blockVertexCount + chunkVertexCount + xaxisVertexCount + yaxisVertexCount + zaxisVertexCount;

		int[] vertices = new int[vertexCount * 4];

		int i = 0;
		int c = 0;

		// blocks
		c = 0x40404040;
		for(int x = -count; x <= count; x++) {
			vertices[i++] = Float.floatToRawIntBits(x * scale);
			vertices[i++] = Float.floatToRawIntBits(y);
			vertices[i++] = Float.floatToRawIntBits(-count * scale);
			vertices[i++] = c;
			vertices[i++] = Float.floatToRawIntBits(x * scale);
			vertices[i++] = Float.floatToRawIntBits(y);
			vertices[i++] = Float.floatToRawIntBits(+count * scale);
			vertices[i++] = c;
		}
		for(int z = -count; z <= count; z++) {
			vertices[i++] = Float.floatToRawIntBits(-count * scale);
			vertices[i++] = Float.floatToRawIntBits(y);
			vertices[i++] = Float.floatToRawIntBits(z * scale);
			vertices[i++] = c;
			vertices[i++] = Float.floatToRawIntBits(+count * scale);
			vertices[i++] = Float.floatToRawIntBits(y);
			vertices[i++] = Float.floatToRawIntBits(z * scale);
			vertices[i++] = c;
		}

		// chunks
		c = 0x80404040;
		for(int x = -count; x <= count; x += blocks) {
			vertices[i++] = Float.floatToRawIntBits(x * scale);
			vertices[i++] = Float.floatToRawIntBits(y);
			vertices[i++] = Float.floatToRawIntBits(-count * scale);
			vertices[i++] = c;
			vertices[i++] = Float.floatToRawIntBits(x * scale);
			vertices[i++] = Float.floatToRawIntBits(y);
			vertices[i++] = Float.floatToRawIntBits(+count * scale);
			vertices[i++] = c;
		}
		for(int z = -count; z <= count; z += blocks) {
			vertices[i++] = Float.floatToRawIntBits(-count * scale);
			vertices[i++] = Float.floatToRawIntBits(y);
			vertices[i++] = Float.floatToRawIntBits(z * scale);
			vertices[i++] = c;
			vertices[i++] = Float.floatToRawIntBits(+count * scale);
			vertices[i++] = Float.floatToRawIntBits(y);
			vertices[i++] = Float.floatToRawIntBits(z * scale);
			vertices[i++] = c;
		}

		// x axis
		c = 0xff0000ff;
		vertices[i++] = Float.floatToRawIntBits(0);
		vertices[i++] = Float.floatToRawIntBits(y);
		vertices[i++] = Float.floatToRawIntBits(0);
		vertices[i++] = c;
		vertices[i++] = Float.floatToRawIntBits(1);
		vertices[i++] = Float.floatToRawIntBits(y);
		vertices[i++] = Float.floatToRawIntBits(0);
		vertices[i++] = c;

		// y axis
		c = 0xff00ff00;
		vertices[i++] = Float.floatToRawIntBits(0);
		vertices[i++] = Float.floatToRawIntBits(y);
		vertices[i++] = Float.floatToRawIntBits(0);
		vertices[i++] = c;
		vertices[i++] = Float.floatToRawIntBits(0);
		vertices[i++] = Float.floatToRawIntBits(y + 1);
		vertices[i++] = Float.floatToRawIntBits(0);
		vertices[i++] = c;

		// z axis
		c = 0xffff0000;
		vertices[i++] = Float.floatToRawIntBits(0);
		vertices[i++] = Float.floatToRawIntBits(y);
		vertices[i++] = Float.floatToRawIntBits(0);
		vertices[i++] = c;
		vertices[i++] = Float.floatToRawIntBits(0);
		vertices[i++] = Float.floatToRawIntBits(y);
		vertices[i++] = Float.floatToRawIntBits(1);
		vertices[i++] = c;

		vertexArray = new Varray();
		vertexArray.create();
		vertexArray.bind();

		BufferDesc vertexBufferDesc = new BufferDesc();
		vertexBufferDesc.setType(BufferType.Vertex);
		vertexBufferDesc.setUsage(BufferUsage.Static);
		vertexBufferDesc.setData(vertexCount * 4, vertices);

		vertexBuffer = new Buffer();
		vertexBuffer.create(vertexBufferDesc);

		GL20.glEnableVertexAttribArray(0);
		GL20.glEnableVertexAttribArray(1);

		GL20.glVertexAttribPointer(0, 3, GL11.GL_FLOAT, false, 16, 0);
		GL20.glVertexAttribPointer(1, 4, GL11.GL_UNSIGNED_BYTE, true, 16, 12);

		Shader vertShader = new Shader();
		vertShader.setPath("shaders/GridVS.glsl");
		vertShader.setType(ShaderType.Vert);
		vertShader.create();

		Shader fragShader = new Shader();
		fragShader.setPath("shaders/GridFS.glsl");
		fragShader.setType(ShaderType.Frag);
		fragShader.create();

		program = new Program();
		program.addShader(vertShader);
		program.addShader(fragShader);
		program.addVattrib(0, "iPosition");
		program.addVattrib(1, "iColor");
		program.create();

		vertShader.destroy();
		fragShader.destroy();

		uniformViewMatrix = program.getUniform("uViewMatrix");
		uniformProjMatrix = program.getUniform("uProjMatrix");

		Varray.unbind();
	}

	public void destroy() {
		if(vertexBuffer != null) {
			vertexBuffer.destroy();
			vertexBuffer = null;
		}

		if(vertexArray != null) {
			vertexArray.destroy();
			vertexArray = null;
		}

		if(program != null) {
			program.destroy();
			program = null;
		}
	}

	public void render() {
		if(program == null || !program.isCreated())
			return;
		View view = View.instance;

		GPU.setDepthTest(false);
		GPU.setDepthMask(true);
		GPU.setBlendMode(BlendMode.Alpha);

		vertexArray.bind();

		program.bind();
		if(uniformViewMatrix != null)
			GL20.glUniformMatrix4fv(uniformViewMatrix.location, false, view.getViewMatrixBuffer());
		if(uniformProjMatrix != null)
			GL20.glUniformMatrix4fv(uniformProjMatrix.location, false, view.getProjMatrixBuffer());

		GL11.glDrawArrays(GL11.GL_LINES, 0, vertexCount);

		Varray.unbind();
	}

	public static Grid instance;

	public static void createInstance() {
		if(instance == null) {
			instance = new Grid();
			instance.create();
		}
	}

	public static void destroyInstance() {
		if(instance != null) {
			instance.destroy();
			instance = null;
		}
	}

	public static void renderInstance() {
		if(instance != null)
			instance.render();
	}
}