package com.indiedev101;

public class Align {
	public static final int None = 0x00000000;

	public static final int Top = 0x00000001;
	public static final int Left = 0x00000002;
	public static final int Right = 0x00000004;
	public static final int Bottom = 0x00000008;

	public static final int CenterX = 0x00000010;
	public static final int CenterY = 0x00000020;
	public static final int Center = 0x00000040;

	public static final int LeftOf = 0x00000100;
	public static final int RightOf = 0x00000200;
	public static final int Above = 0x00000400;
	public static final int Below = 0x00000800;

	public static final int TopOfCenter = 0x00001000;
	public static final int LeftOfCenter = 0x00002000;
	public static final int RightOfCenter = 0x00004000;
	public static final int BottomOfCenter = 0x00008000;

	public static final int TopLeft = Top | Left;
	public static final int TopRight = Top | Right;
	public static final int TopCenter = Top | CenterX;
	public static final int BottomLeft = Bottom | Left;
	public static final int BottomRight = Bottom | Right;
	public static final int BottomCenter = Bottom | CenterY;

	public static final int Front = 0x00010000;
	public static final int Back = 0x00020000;
	public static final int Infront = 0x00040000;
	public static final int Behind = 0x00080000;
}