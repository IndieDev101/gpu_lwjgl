package com.indiedev101;

import java.io.File;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;

public class Font {
	private boolean created;
	private boolean bound;

	private final int numIntsPerVert = 5;
	private final int numIntsPerQuad = numIntsPerVert * 4;
	private final int maxInts = numIntsPerVert * 8192;
	private int curInt;
	private int[] ints;
	private IntBuffer intBuffer;
	private Varray vertexArray;
	private Buffer vertexBuffer;
	private Buffer indexBuffer;

	private Program program;
	private Uniform uniformScreenMatrix;

	private Texture texture;
	private int glyphWidth;
	private int glyphHeight;
	private static final int glyphsPerRow = 16;
	private Glyph[] glyphs = new Glyph[256];
	private float spaceScale = 0.4f;
	private float spacingScale = 0.8f;

	public void create() {
		if(created)
			return;

		ints = new int[maxInts];
		intBuffer = BufferUtils.createIntBuffer(maxInts);

		vertexArray = new Varray();
		vertexArray.create();
		vertexArray.bind();

		BufferDesc vertexBufferDesc = new BufferDesc();
		vertexBufferDesc.setType(BufferType.Vertex);
		vertexBufferDesc.setUsage(BufferUsage.Dynamic);
		vertexBufferDesc.setSize(maxInts * 4);

		vertexBuffer = new Buffer();
		vertexBuffer.create(vertexBufferDesc);

		int numIndices = (65536 / 4) * 6;
		short[] indices = new short[numIndices];
		for(int idx = 0, vtx = 4; vtx <= 65536; vtx += 4) {
			short v4 = (short)(vtx - 4);
			short v3 = (short)(vtx - 3);
			short v2 = (short)(vtx - 2);
			short v1 = (short)(vtx - 1);
			indices[idx++] = v4;
			indices[idx++] = v3;
			indices[idx++] = v2;
			indices[idx++] = v4;
			indices[idx++] = v2;
			indices[idx++] = v1;
		}

		BufferDesc indexBufferDesc = new BufferDesc();
		indexBufferDesc.setType(BufferType.Index);
		indexBufferDesc.setUsage(BufferUsage.Static);
		indexBufferDesc.setData(numIndices, indices);

		indexBuffer = new Buffer();
		indexBuffer.create(indexBufferDesc);

		GL20.glEnableVertexAttribArray(0);
		GL20.glEnableVertexAttribArray(1);
		GL20.glEnableVertexAttribArray(2);

		GL20.glVertexAttribPointer(0, 2, GL11.GL_FLOAT, false, 20, 0);
		GL20.glVertexAttribPointer(1, 4, GL11.GL_UNSIGNED_BYTE, true, 20, 8);
		GL20.glVertexAttribPointer(2, 2, GL11.GL_FLOAT, false, 20, 12);

		Shader vertShader = new Shader();
		vertShader.setPath("shaders/FontVS.glsl");
		vertShader.setType(ShaderType.Vert);
		vertShader.create();

		Shader fragShader = new Shader();
		fragShader.setPath("shaders/FontFS.glsl");
		fragShader.setType(ShaderType.Frag);
		fragShader.create();

		program = new Program();
		program.addShader(vertShader);
		program.addShader(fragShader);
		program.addVattrib(0, "iPosition");
		program.addVattrib(1, "iColor");
		program.addVattrib(2, "iTexCoord");
		program.addSampler(0, "uTexture");
		program.create();

		vertShader.destroy();
		fragShader.destroy();

		uniformScreenMatrix = program.getUniform("uScreenMatrix");

		Varray.unbind();

		String path = IO.getFilePath("textures/Font.png");
		Image image = PNGImage.load(path);
		if(image == null) {
			Log.error("Failed to load %s\n", path);
			return;
		}

		TextureDesc textureDesc = new TextureDesc();
		textureDesc.width = image.width;
		textureDesc.height = image.height;
		textureDesc.data = image.dataAsByteBuffer;

		texture = new Texture();
		texture.create(textureDesc);

		glyphWidth = texture.width / glyphsPerRow;
		glyphHeight = texture.height / glyphsPerRow;

		for(int i = 0; i < 256; i++) {
			int x = (i % glyphsPerRow);
			int y = (i / glyphsPerRow);
			y = glyphsPerRow - y - 1;
			x *= glyphHeight;
			y *= glyphHeight;

			int x1 = TextureUtil.minX(x, y, glyphWidth, glyphHeight, image.data, image.width);
			if(x1 >= x + glyphWidth)
				continue;

			int y1 = TextureUtil.minY(x, y, glyphWidth, glyphHeight, image.data, image.width);
			int x2 = TextureUtil.maxX(x, y, glyphWidth, glyphHeight, image.data, image.width);
			int y2 = TextureUtil.maxY(x, y, glyphWidth, glyphHeight, image.data, image.width);

			assert x2 >= x;
			assert y1 < y + glyphHeight;
			assert y2 >= y;

			Glyph glyph = glyphs[i] = new Glyph();
			glyph.originX = x;
			glyph.originY = y;
			glyph.offsetX = x1 - x;
			glyph.offsetY = y1 - y;
			glyph.width = x2 - x1 + 1;
			glyph.height = y2 - y1 + 1;

			//Log.debug("'%c': %d,%d %d,%d %dx%d\n", (char)i, glyph.originX, glyph.originY, glyph.offsetX, glyph.offsetY, glyph.width, glyph.height);
		}

		created = true;
	}

	public void destroy() {
		if(program != null) {
			program.destroy();
			program = null;
		}

		if(indexBuffer != null) {
			indexBuffer.destroy();
			indexBuffer = null;
		}

		if(vertexBuffer != null) {
			vertexBuffer.destroy();
			vertexBuffer = null;
		}

		if(vertexArray != null) {
			vertexArray.destroy();
			vertexArray = null;
		}

		intBuffer = null;
		ints = null;

		for(int i = 0; i < 256; i++)
			glyphs[i] = null;

		if(texture != null) {
			texture.destroy();
			texture = null;
		}

		created = false;
	}

	public boolean bind() {
		if(!created)
			return false;
		if(bound)
			return true;
		View view = View.instance;

		GPU.setDepthTest(false);
		GPU.setDepthMask(false);
		GPU.setBlendMode(BlendMode.Alpha);

		vertexArray.bind();
		vertexBuffer.bind();
		
		program.bind();
		GL20.glUniformMatrix4fv(uniformScreenMatrix.location, false, view.getScreenMatrixBuffer());

		texture.bind(0);

		reset();

		bound = true;
		return true;
	}

	public void unbind() {
		if(!bound)
			return;

		flush();

		Varray.unbind();

		bound = false;
	}

	private void reset() {
		curInt = 0;
	}

	private void flush() {
		if(curInt == 0)
			return;

		intBuffer.clear();
		intBuffer.put(ints, 0, curInt);
		intBuffer.flip();

		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, maxInts * 4, GL15.GL_STREAM_DRAW);
		GL15.glBufferSubData(GL15.GL_ARRAY_BUFFER, 0, intBuffer);

		int vertexCount = curInt / numIntsPerVert;
		int indexCount = (vertexCount / 4) * 6;
		GL12.glDrawRangeElements(GL11.GL_TRIANGLES, 0, vertexCount, indexCount, GL11.GL_UNSIGNED_SHORT, 0);

		reset();
	}

	private void grow() {
		if(curInt + numIntsPerQuad > maxInts)
			flush();
	}

	private void vert(float x, float y, int c, float u, float v) {
		ints[curInt++] = Float.floatToRawIntBits(x);
		ints[curInt++] = Float.floatToRawIntBits(y);
		ints[curInt++] = c;
		ints[curInt++] = Float.floatToRawIntBits(u);
		ints[curInt++] = Float.floatToRawIntBits(v);
	}

	public float getTextWidth(String text, float scale) {
		if(!created)
			return 0;

		float x = 0;
		int numQuads = 0;

		for(int i = 0; i < text.length(); i++) {
			char c = text.charAt(i);
			if(c == ' ') {
				x += (float)(int)(glyphWidth * scale * spaceScale);
				continue;
			}

			int j = (int)c;
			Glyph glyph = glyphs[j];
			if(glyph == null || glyph.width == 0 || glyph.height == 0)
				continue;

			if(numQuads++ > 0) {
				float o = (float)(int)(spacingScale * scale);
				if(o < 1)
					o = 1;
				x += o;
			}
			x += (float)(int)(glyph.width * scale);
		}

		return x;
	}

	public float getTextHeight(float scale) {
		if(!created)
			return 0;

		return glyphHeight * scale;
	}

	public float addRect(float x, float y, float w, float h, int color, int alignX, int alignY) {
		if(!bound)
			return x;

		if(alignX == Align.Center)
			x -= w / 2.0f;
		else if(alignX == Align.Right)
			x -= w;

		if(alignY == Align.Center)
			y -= h / 2.0f;
		else if(alignY == Align.Top)
			y -= h;

		float x1 = x;
		float y1 = y;
		float x2 = x1 + w;
		float y2 = y1 + h;

		float u1 = 0;
		float v1 = 0;
		float u2 = 1.0f / (float)texture.width;
		float v2 = 1.0f / (float)texture.height;

		grow();
		vert(x1, y1, color, u1, v1);
		vert(x2, y1, color, u2, v1);
		vert(x2, y2, color, u2, v2);
		vert(x1, y2, color, u1, v2);

		x += w;

		return x;
	}

	public float addText(String text, float x, float y, float scale, int color, int alignX, int alignY) {
		if(!bound || text.length() == 0)
			return x;

		if(alignX == Align.Center)
			x -= getTextWidth(text, scale) / 2.0f;
		else if(alignX == Align.Right)
			x -= getTextWidth(text, scale);

		if(alignY == Align.Center)
			y -= getTextHeight(scale) / 2.0f;
		else if(alignY == Align.Top)
			y -= getTextHeight(scale);

		x = (float)(int)x;
		y = (float)(int)y;

		int numQuads = 0;

		for(int i = 0; i < text.length(); i++) {
			char c = text.charAt(i);
			if(c == ' ') {
				x += (float)(int)(glyphWidth * scale * spaceScale);
				continue;
			}

			int j = (int)c;
			if(j < 0 || j >= 256)
				continue;

			Glyph glyph = glyphs[j];
			if(glyph == null || glyph.width == 0 || glyph.height == 0)
				continue;

			if(numQuads++ > 0) {
				float a = (float)(int)(spacingScale * scale);
				if(a < 1)
					a = 1;
				x += a;
			}

			float x1 = x + (glyph.offsetX * scale);
			float y1 = y + (glyph.offsetY * scale);
			float x2 = x1 + (glyph.width * scale);
			float y2 = y1 + (glyph.height * scale);

			float u1 = (float)(glyph.originX + glyph.offsetX) / (float)texture.width;
			float v1 = (float)(glyph.originY + glyph.offsetY) / (float)texture.height;
			float u2 = u1 + (float)glyph.width / (float)texture.width;
			float v2 = v1 + (float)glyph.height / (float)texture.height;

			grow();
			vert(x1, y1, color, u1, v1);
			vert(x2, y1, color, u2, v1);
			vert(x2, y2, color, u2, v2);
			vert(x1, y2, color, u1, v2);

			x += (float)(int)(x2 - x1);
		}

		return x;
	}

	public static Font instance;

	public static void createInstance() {
		if(instance == null) {
			instance = new Font();
			instance.create();
		}
	}

	public static void destroyInstance() {
		if(instance != null) {
			instance.destroy();
			instance = null;
		}
	}
}