package com.indiedev101;

import java.util.HashMap;

public abstract class Effect {
	private HashMap<Integer, EffectShader> shaders = new HashMap<Integer, EffectShader>();
	private String name;
	private String file;
	private String source;

	public Effect(String name, String file) {
		this.name = name;
		this.file = file;
	}

	public void create() {
		String path = IO.getFilePath("shaders/" + file);
		source = IO.getFileAsString(path);
	}

	public void destroy() {
		for(EffectShader shader : shaders.values())
			shader.destroy();
		shaders.clear();
		source = null;
	}

	public EffectShader getShader(int hash) {
		Integer key = Integer.valueOf(hash);
		EffectShader shader = shaders.get(key);
		if(shader == null) {
			String name = String.format("%s_%d", this.name, hash);
			shader = newShader(name);
			initShader(shader, hash);
			shader.create(source);
			shaders.put(hash, shader);
		}
		return shader;
	}

	public EffectShader newShader(String name) {
		return new EffectShader(name);
	}

	public abstract void initShader(EffectShader shader, int hash);

	public static Effect destroyEffect(Effect effect) {
		if(effect != null)
			effect.destroy();
		return null;
	}
}