package com.indiedev101;

public class EffectSampler {
	public String name;
	public int index;

	public EffectSampler(String name, int index) {
		this.name = name;
		this.index = index;
	}

	public static final int Tex0 = 0;
	public static final int Tex1 = 1;
	public static final int Tex2 = 2;
	public static final int Tex3 = 3;
	public static final String[] names = new String[] { "uTexture", "uTexture1", "uTexture2", "uTexture3" };
}