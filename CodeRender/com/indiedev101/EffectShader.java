package com.indiedev101;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

public class EffectShader {
	private int id;
	private final HashMap<String, EffectParam> params = new HashMap<String, EffectParam>();

	private final String name;
	private String version;
	private ArrayList<EffectDefine> defines;
	private ArrayList<EffectAttrib> attribs;
	private ArrayList<EffectSampler> samplers;

	public EffectShader(String name) {
		this.name = name;
	}

	public void create(String source) {
		int id = GL20.glCreateProgram();

		int vertShader = EffectUtils.createShader(GL20.GL_VERTEX_SHADER, source, version, defines);
		int fragShader = EffectUtils.createShader(GL20.GL_FRAGMENT_SHADER, source, version, defines);

		if(vertShader > 0)
			GL20.glAttachShader(id, vertShader);
		if(fragShader > 0)
			GL20.glAttachShader(id, fragShader);

		if(attribs != null) {
			for(int i = 0; i < attribs.size(); i++) {
				EffectAttrib attrib = attribs.get(i);
				GL20.glBindAttribLocation(id, attrib.index, attrib.name);
			}
		}

		boolean br = false;

		GL20.glLinkProgram(id);
		int linkStatus = GL20.glGetProgrami(id, GL20.GL_LINK_STATUS);
		if(linkStatus == GL11.GL_TRUE) {

			GL20.glValidateProgram(id);
			int validateStatus = GL20.glGetProgrami(id, GL20.GL_VALIDATE_STATUS);
			if(validateStatus == GL11.GL_TRUE) {

				GL20.glUseProgram(id);

				if(samplers != null) {
					for(int i = 0; i < samplers.size(); i++) {
						EffectSampler sampler = samplers.get(i);
						int uniform = GL20.glGetUniformLocation(id, sampler.name);
						if(uniform != -1)
							GL20.glUniform1i(uniform, sampler.index);
					}
				}

				for(Map.Entry<String, EffectParam> i : params.entrySet())
					i.getValue().uniform = GL20.glGetUniformLocation(id, i.getKey());

				br = true;
			} else {
				Log.error("%s\n", GL20.glGetProgramInfoLog(id, 1000));
			}
		} else {
			Log.error("%s\n", GL20.glGetProgramInfoLog(id, 1000));
		}

		if(fragShader > 0)
			GL20.glDetachShader(id, fragShader);
		if(vertShader > 0)
			GL20.glDetachShader(id, vertShader);

		if(fragShader > 0)
			GL20.glDeleteShader(fragShader);
		if(vertShader > 0)
			GL20.glDeleteShader(vertShader);

		if(br) {
			if(this.id != 0)
				GL20.glDeleteProgram(this.id);
			this.id = id;
			getParams();
		} else {
			GL20.glDeleteProgram(id);
		}
	}

	public void destroy() {
		if(id != 0) {
			GL20.glDeleteProgram(id);
			id = 0;
		}
		params.clear();

		version = null;
		defines = null;
		attribs = null;
		samplers = null;
	}

	public void bind() {
		GL20.glUseProgram(id);
	}

	public void unbind() {
		GL20.glUseProgram(0);
	}

	public EffectParam getParam(String name, boolean warnIfNotExists) {
		if(id == 0)
			return null;
		EffectParam param = params.get(name);
		if(param == null) {
			int uniform = GL20.glGetUniformLocation(id, name);
			if(uniform >= 0) {
				param = new EffectParam(uniform);
				params.put(name, param);
			} else if(warnIfNotExists) {
				Log.debug("Failed to find shader param %s in technique %s\n", name, this.name);
			}
		}
		return param;
	}

	public EffectParam getParam(String name) {
		return getParam(name, false);
	}

	public void getParams() {
	}

	public void addDefine(String name, String value) {
		if(defines == null)
			defines = new ArrayList<EffectDefine>();
		defines.add(new EffectDefine(name, value));
	}

	public void addAttrib(String name, int index) {
		if(attribs == null)
			attribs = new ArrayList<EffectAttrib>();
		attribs.add(new EffectAttrib(name, index));
	}

	public void addSampler(String name, int index) {
		if(samplers == null)
			samplers = new ArrayList<EffectSampler>();
		samplers.add(new EffectSampler(name, index));
	}

	public void addDefine(String name) {
		addDefine(name, null);
	}

	public void addAttrib(int type) {
		addAttrib(EffectAttrib.names[type], type);
	}

	public void addSampler(int type) {
		addSampler(EffectSampler.names[type], type);
	}

	private IntBuffer intBuffer;

	private final void setIntBuffer(int num, int[] val) {
		if(intBuffer == null || num > intBuffer.capacity())
			intBuffer = BufferUtils.createIntBuffer(num);
		intBuffer.clear();
		intBuffer.put(val);
		intBuffer.flip();
		intBuffer.limit(num);
	}

	private FloatBuffer floatBuffer;

	private final void setFloatBuffer(int num, float[] val) {
		if(floatBuffer == null || num > floatBuffer.capacity())
			floatBuffer = BufferUtils.createFloatBuffer(num);
		floatBuffer.clear();
		floatBuffer.put(val, 0, num);
		floatBuffer.flip();
		floatBuffer.limit(num);
	}

	public void setMat4(int index, FloatBuffer val) {
		if(index != -1)
			GL20.glUniformMatrix4fv(index, false, val);
	}

	public void setMat4(int index, float[] val) {
		if(index != -1) {
			setFloatBuffer(16, val);
			GL20.glUniformMatrix4fv(index, false, floatBuffer);
		}
	}

	public void setMat4(int index, Mat4f val) {
		if(index != -1) {
			setFloatBuffer(16, val.m);
			GL20.glUniformMatrix4fv(index, false, floatBuffer);
		}
	}

	public void setMat4(EffectParam param, FloatBuffer val) {
		if(param != null)
			setMat4(param.uniform, val);
	}

	public void setMat4(EffectParam param, float[] val) {
		if(param != null)
			setMat4(param.uniform, val);
	}

	public void setMat4(EffectParam param, Mat4f val) {
		if(param != null)
			setMat4(param.uniform, val);
	}

	public void setMat4(String name, FloatBuffer val) {
		setMat4(getParam(name), val);
	}

	public void setMat4(String name, float[] val) {
		setMat4(getParam(name), val);
	}

	public void setMat4(String name, Mat4f val) {
		setMat4(getParam(name), val);
	}
}