package com.indiedev101;

public class EffectAttrib {
	public String name;
	public int index;

	public EffectAttrib(String name, int index) {
		this.name = name;
		this.index = index;
	}

	public static final int Pos = 0;
	public static final int Nor = 1;
	public static final int Col = 2;
	public static final int Tex = 3;
	public static final String[] names = new String[] { "aPosition", "aNormal", "aColor", "aTexCoord" };
}