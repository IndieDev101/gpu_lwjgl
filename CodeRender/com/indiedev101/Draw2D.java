package com.indiedev101;

import java.io.File;
import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;

public class Draw2D {
	private boolean created;
	private boolean begun;
	private int primType;

	private final int numIntsPerVert = 5;
	private final int numIntsPerQuad = numIntsPerVert * 4;
	private final int maxInts = numIntsPerVert * 8192;
	private int curInt;
	private int[] ints;
	private IntBuffer intBuffer;
	private Varray vertexArray;
	private Buffer vertexBuffer;

	private Program program;
	private Uniform uniformScreenMatrix;

	public void create() {
		if(created)
			return;

		ints = new int[maxInts];
		intBuffer = BufferUtils.createIntBuffer(maxInts);

		vertexArray = new Varray();
		vertexArray.create();
		vertexArray.bind();

		BufferDesc vertexBufferDesc = new BufferDesc();
		vertexBufferDesc.setType(BufferType.Vertex);
		vertexBufferDesc.setUsage(BufferUsage.Dynamic);
		vertexBufferDesc.setSize(maxInts * 4);

		vertexBuffer = new Buffer();
		vertexBuffer.create(vertexBufferDesc);
		GPU.indexBuffer.bind();

		GL20.glEnableVertexAttribArray(0);
		GL20.glEnableVertexAttribArray(1);
		GL20.glEnableVertexAttribArray(2);

		GL20.glVertexAttribPointer(0, 2, GL11.GL_FLOAT, false, 20, 0);
		GL20.glVertexAttribPointer(1, 4, GL11.GL_UNSIGNED_BYTE, true, 20, 8);
		GL20.glVertexAttribPointer(2, 2, GL11.GL_FLOAT, false, 20, 12);

		Shader vertShader = new Shader();
		vertShader.setPath("shaders/Draw2DVS.glsl");
		vertShader.setType(ShaderType.Vert);
		vertShader.create();

		Shader fragShader = new Shader();
		fragShader.setPath("shaders/DrawFS.glsl");
		fragShader.setType(ShaderType.Frag);
		fragShader.create();

		program = new Program();
		program.addShader(vertShader);
		program.addShader(fragShader);
		program.addVattrib(0, "iPosition");
		program.addVattrib(1, "iColor");
		program.addVattrib(2, "iTexCoord");
		program.addSampler(0, "uTexture");
		program.create();

		vertShader.destroy();
		fragShader.destroy();

		uniformScreenMatrix = program.getUniform("uScreenMatrix");

		Varray.unbind();

		created = true;
	}

	public void destroy() {
		if(program != null) {
			program.destroy();
			program = null;
		}

		if(vertexBuffer != null) {
			vertexBuffer.destroy();
			vertexBuffer = null;
		}

		if(vertexArray != null) {
			vertexArray.destroy();
			vertexArray = null;
		}

		intBuffer = null;
		ints = null;

		created = false;
	}

	public boolean begin(int primType) {
		if(!created)
			return false;
		if(begun)
			return true;
		View view = View.instance;

		this.primType = primType;

		vertexArray.bind();
		vertexBuffer.bind();

		program.bind();
		GL20.glUniformMatrix4fv(uniformScreenMatrix.location, false, view.getScreenMatrixBuffer());

		reset();

		begun = true;
		return true;
	}

	public boolean begin() {
		return begin(PrimType.Quads);
	}

	public void end() {
		if(!begun)
			return;

		flush();

		Varray.unbind();

		begun = false;
	}

	private void reset() {
		curInt = 0;
	}

	public void flush() {
		if(curInt == 0)
			return;

		intBuffer.clear();
		intBuffer.put(ints, 0, curInt);
		intBuffer.flip();

		int numBytes = Mathi.nextPowerOf2(curInt * 4);
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, numBytes, GL15.GL_STREAM_DRAW);
		GL15.glBufferSubData(GL15.GL_ARRAY_BUFFER, 0, intBuffer);

		int vertexCount = curInt / numIntsPerVert;
		if(primType == PrimType.Quads) {
			GPU.indexBuffer.bind();
			int indexCount = (vertexCount / 4) * 6;
			GL12.glDrawRangeElements(GL11.GL_TRIANGLES, 0, vertexCount, indexCount, GL11.GL_UNSIGNED_SHORT, 0);
		} else if(primType == PrimType.Lines || primType == PrimType.LineStrip || primType == PrimType.Points) {
			GPU.indexBuffer.unbind();
			GPU.whiteTexture.bind(0);
			GL11.glDrawArrays(primType, 0, vertexCount);
		}

		reset();
	}

	private void grow() {
		if(curInt + numIntsPerQuad > maxInts)
			flush();
	}

	public void vert(float x, float y, int c, float u, float v) {
		int r = (c & 0x00ff0000) >> 16;
		int g = (c & 0x0000ff00) >> 8;
		int b = (c & 0x000000ff);
		int a = (c & 0xff000000) >>> 24;
		ints[curInt++] = Float.floatToRawIntBits(x);
		ints[curInt++] = Float.floatToRawIntBits(y);
		ints[curInt++] = a << 24 | b << 16 | g << 8 | r;
		//ints[curInt++] = c;
		ints[curInt++] = Float.floatToRawIntBits(u);
		ints[curInt++] = Float.floatToRawIntBits(v);
	}

	public void vert(float x, float y, int c) {
		vert(x, y, c, 0, 0);
	}

	public void addLine(float x1, float y1, float x2, float y2, int color) {
		if(!begun)
			return;

		grow();
		vert(x1, y1, color, 0, 0);
		vert(x2, y2, color, 0, 0);
	}

	public void addRect(float x, float y, float w, float h, int color, float s, float u1, float v1, float u2, float v2, int alignX, int alignY) {
		if(!begun)
			return;

		float x1 = x;
		float y1 = y;
		if(alignX == Align.Right)
			x1 -= (w * s);
		else if(alignX == Align.Center)
			x1 -= (w * s) / 2;
		if(alignY == Align.Top)
			y1 -= (h * s);
		else if(alignY == Align.Center)
			y1 -= (h * s) / 2;
		float x2 = x1 + (w * s);
		float y2 = y1 + (h * s);

		grow();
		vert(x1, y1, color, u1, 1 - v2);
		vert(x2, y1, color, u2, 1 - v2);
		vert(x2, y2, color, u2, 1 - v1);
		vert(x1, y2, color, u1, 1 - v1);
	}

	public void addRect(float x, float y, float w, float h, int c, float s, float u, float v, Texture texture, int alignX, int alignY) {
		if(texture == null)
			return;
		float u1 = (float)u / (float)texture.width;
		float v1 = (float)v / (float)texture.height;
		float u2 = (float)(u + w) / (float)texture.width;
		float v2 = (float)(v + h) / (float)texture.height;
		addRect(x, y, w, h, c, s, u1, v1, u2, v2, alignX, alignY);
	}

	public void addColFillRect(float x, float y, float w, float h, int c, float s, int alignX, int alignY) {
		float x1 = x;
		float y1 = y;
		if(alignX == Align.Right)
			x1 -= (w * s);
		else if(alignX == Align.Center || alignX == Align.CenterX)
			x1 -= (w * s) / 2;
		if(alignY == Align.Top)
			y1 -= (h * s);
		else if(alignY == Align.Center || alignY == Align.CenterY)
			y1 -= (h * s) / 2;
		float x2 = x1 + (w * s);
		float y2 = y1 + (h * s);

		grow();
		vert(x1, y1, c, 0, 0);
		vert(x2, y1, c, 0, 0);
		vert(x2, y2, c, 0, 0);
		vert(x1, y2, c, 0, 0);
	}

	public void addColFillRect(float x, float y, float w, float h, float b, int c, float s, int alignX, int alignY) {
		addColFillRect(x + b, y + b, w - b - b, h - b - b, c, s, alignX, alignY);
	}

	public void addColLineRect(float x, float y, float w, float h, int c, float s, int alignX, int alignY) {
		float x1 = x;
		float y1 = y;
		if(alignX == Align.Right)
			x1 -= (w * s);
		else if(alignX == Align.Center || alignX == Align.CenterX)
			x1 -= (w * s) / 2;
		if(alignY == Align.Top)
			y1 -= (h * s);
		else if(alignY == Align.Center || alignY == Align.CenterY)
			y1 -= (h * s) / 2;
		float x2 = x1 + (w * s);
		float y2 = y1 + (h * s);

		grow();
		vert(x1, y1, c, 0, 0);
		vert(x2, y1, c, 0, 0);
		vert(x2, y1, c, 0, 0);
		vert(x2, y2, c, 0, 0);
		vert(x2, y2, c, 0, 0);
		vert(x1, y2, c, 0, 0);
		vert(x1, y2, c, 0, 0);
		vert(x1, y1, c, 0, 0);
	}

	public static Draw2D instance;

	public static void createInstance() {
		if(instance == null) {
			instance = new Draw2D();
			instance.create();
		}
	}

	public static void destroyInstance() {
		if(instance != null) {
			instance.destroy();
			instance = null;
		}
	}
}