package com.indiedev101;

public class FPS {
	private int frameCount;
	private float frameCycle;
	private int frameIndex;
	private float frameRate;
	private final float[] frameTimes = new float[60 * 3];

	private int x;
	private int y;
	private int h;
	private int s;
	public boolean showText = true;
	public boolean showBars = true;

	public void update(float timeStep) {
		frameTimes[frameIndex] = timeStep;
		if(++frameIndex >= frameTimes.length)
			frameIndex = 0;

		frameCount++;
		frameCycle += timeStep;
		if(frameCycle > 0.25f) {
			frameRate = (float)frameCount / frameCycle;
			frameCount = 0;
			frameCycle = 0;
		}

		s = (int)GPU.scale;
		x = 8 * s;
		y = 8 * s;
		Font font = Font.instance;
		h = font != null ? (int)font.getTextHeight(s) : 8 * s;
	}

	public void render(Font font, String text) {
		if(!showText)
			return;

		int fps = (int)Math.ceil(frameRate);
		if(text != null)
			text = String.format("%s %d fps", text, fps);
		else
			text = String.format("%d fps", fps);
		x += font.addText(text, x, y, s, 0xffffffff, Align.None, Align.None);
	}

	public void render() {
		if(!showBars)
			return;

		Draw2D draw = Draw2D.instance;
		draw.begin(PrimType.LineStrip);

		float maxFps = 60;
		float badFps = maxFps - 5;
		int j = frameIndex + 1;
		int n = frameTimes.length;

		for(int i = 0; i < n; i++, j++) {
			if(j >= n)
				j = 0;

			float fps = 1.0f / frameTimes[j];
			if(fps > maxFps)
				fps = maxFps;

			int c = fps < badFps ? 0xffff0000 : 0xff00ff00;

			fps /= maxFps;
			fps *= h * 0.7f;

			draw.vert(x, y + fps, c, 0, 0);

			x += s;
		}

		draw.end();
	}

	public static FPS instance;

	public static void createInstance() {
		if(instance == null)
			instance = new FPS();
	}

	public static void destroyInstance() {
		if(instance != null)
			instance = null;
	}

	public static void updateInstance(float timeStep) {
		if(instance != null)
			instance.update(timeStep);
	}

	public static void renderInstance(Font font, String text) {
		if(instance != null)
			instance.render(font, text);
	}

	public static void renderInstance() {
		if(instance != null)
			instance.render();
	}
}