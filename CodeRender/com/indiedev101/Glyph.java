package com.indiedev101;

public class Glyph {
	public int originX;
	public int originY;
	public int offsetX;
	public int offsetY;
	public int width;
	public int height;
}