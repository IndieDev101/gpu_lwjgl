package com.indiedev101;

public class EffectDefine {
	public String name;
	public String value;

	public EffectDefine(String name, String value) {
		this.name = name;
		this.value = value;
	}
}