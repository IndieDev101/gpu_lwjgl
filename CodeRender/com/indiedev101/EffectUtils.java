package com.indiedev101;

import java.util.ArrayList;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL32;

public class EffectUtils {
	public static int createShader(int type, String source, String version, ArrayList<EffectDefine> defines) {
		String string = "";

		string += "#version ";
		if(version != null)
			string += version;
		else
			string += "120";
		string += "\n";

		string += "#define ";
		if(type == GL32.GL_GEOMETRY_SHADER)
			string += "GEOM";
		else if(type == GL20.GL_VERTEX_SHADER)
			string += "VERT";
		else if(type == GL20.GL_FRAGMENT_SHADER)
			string += "FRAG";
		else
			return 0;
		string += "\n";

		if(defines != null) {
			for(int i = 0; i < defines.size(); i++) {
				EffectDefine define = defines.get(i);
				string += "#define ";
				string += define.name;
				if(define.value != null) {
					string += " ";
					string += define.value;
				}
				string += "\n";
			}
		}

		if(source != null)
			string += source;

		int id = GL20.glCreateShader(type);
		GL20.glShaderSource(id, string);
		GL20.glCompileShader(id);

		int compileStatus = GL20.glGetShaderi(id, GL20.GL_COMPILE_STATUS);
		if(compileStatus == GL11.GL_FALSE) {
			Log.debug("******************************\n");
			printShader(string);
			Log.debug("******************************\n");
			Log.error("%s\n", GL20.glGetShaderInfoLog(id, 1000));
			GL20.glDeleteShader(id);
			return 0;
		}

		return id;
	}

	public static void printShader(String string) {
		int l = 0;
		int i = 0;
		while(i < string.length()) {
			int j = i;
			while(j < string.length()) {
				if(string.charAt(j) == 10) {
					break;
				} else {
					j++;
				}
			}
			if(j >= i && j < string.length()) {
				Log.debug("%d ", l);
				for(int k = i; k <= j; k++) {
					Log.debug("%c", string.charAt(k));
				}
				l++;
			}
			i = j + 1;
		}
	}
}