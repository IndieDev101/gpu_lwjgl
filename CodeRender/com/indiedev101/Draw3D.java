package com.indiedev101;

import java.io.File;
import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;

public class Draw3D {
	private boolean created;
	private boolean begun;
	private int primType;

	private final int numIntsPerVert = 6;
	private final int numIntsPerQuad = numIntsPerVert * 4;
	private final int maxInts = numIntsPerVert * 8192;
	private int curInt;
	private int[] ints;
	private IntBuffer intBuffer;
	private Varray vertexArray;
	private Buffer vertexBuffer;

	private Program program;
	private Uniform uniformViewMatrix;
	private Uniform uniformProjMatrix;

	public void create() {
		if(created)
			return;

		ints = new int[maxInts];
		intBuffer = BufferUtils.createIntBuffer(maxInts);

		vertexArray = new Varray();
		vertexArray.create();
		vertexArray.bind();

		BufferDesc vertexBufferDesc = new BufferDesc();
		vertexBufferDesc.setType(BufferType.Vertex);
		vertexBufferDesc.setUsage(BufferUsage.Dynamic);
		vertexBufferDesc.setSize(maxInts * 4);

		vertexBuffer = new Buffer();
		vertexBuffer.create(vertexBufferDesc);
		GPU.indexBuffer.bind();

		GL20.glEnableVertexAttribArray(0);
		GL20.glEnableVertexAttribArray(1);
		GL20.glEnableVertexAttribArray(2);

		GL20.glVertexAttribPointer(0, 3, GL11.GL_FLOAT, false, 24, 0);
		GL20.glVertexAttribPointer(1, 4, GL11.GL_UNSIGNED_BYTE, true, 24, 12);
		GL20.glVertexAttribPointer(2, 2, GL11.GL_FLOAT, false, 24, 16);

		Shader vertShader = new Shader();
		vertShader.setPath("shaders/Draw3DVS.glsl");
		vertShader.setType(ShaderType.Vert);
		vertShader.create();

		Shader fragShader = new Shader();
		fragShader.setPath("shaders/DrawFS.glsl");
		fragShader.setType(ShaderType.Frag);
		fragShader.create();

		program = new Program();
		program.addShader(vertShader);
		program.addShader(fragShader);
		program.addVattrib(0, "iPosition");
		program.addVattrib(1, "iColor");
		program.addVattrib(2, "iTexCoord");
		program.addSampler(0, "uTexture");
		program.create();

		vertShader.destroy();
		fragShader.destroy();

		uniformViewMatrix = program.getUniform("uViewMatrix");
		uniformProjMatrix = program.getUniform("uProjMatrix");

		Varray.unbind();

		created = true;
	}

	public void destroy() {
		if(program != null) {
			program.destroy();
			program = null;
		}

		if(vertexBuffer != null) {
			vertexBuffer.destroy();
			vertexBuffer = null;
		}

		if(vertexArray != null) {
			vertexArray.destroy();
			vertexArray = null;
		}

		intBuffer = null;
		ints = null;

		created = false;
	}

	public boolean begin(int primType) {
		if(!created)
			return false;
		if(begun)
			return true;
		View view = View.instance;

		this.primType = primType;

		vertexArray.bind();
		vertexBuffer.bind();

		program.bind();
		GL20.glUniformMatrix4fv(uniformViewMatrix.location, false, view.getViewMatrixBuffer());
		GL20.glUniformMatrix4fv(uniformProjMatrix.location, false, view.getProjMatrixBuffer());

		reset();

		begun = true;
		return true;
	}

	public boolean begin() {
		return begin(PrimType.Quads);
	}

	public void end() {
		if(!begun)
			return;

		flush();

		Varray.unbind();

		begun = false;
	}

	private void reset() {
		curInt = 0;
	}

	public void flush() {
		if(curInt == 0)
			return;

		intBuffer.clear();
		intBuffer.put(ints, 0, curInt);
		intBuffer.flip();

		int numBytes = Mathi.nextPowerOf2(curInt * 4);
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, numBytes, GL15.GL_STREAM_DRAW);
		GL15.glBufferSubData(GL15.GL_ARRAY_BUFFER, 0, intBuffer);

		int vertexCount = curInt / numIntsPerVert;
		if(primType == PrimType.Quads) {
			GPU.indexBuffer.bind();
			int indexCount = (vertexCount / 4) * 6;
			GL12.glDrawRangeElements(GL11.GL_TRIANGLES, 0, vertexCount, indexCount, GL11.GL_UNSIGNED_SHORT, 0);
		} else if(primType == PrimType.Lines || primType == PrimType.LineStrip || primType == PrimType.Points) {
			GPU.indexBuffer.unbind();
			GPU.whiteTexture.bind(0);
			GL11.glDrawArrays(primType, 0, vertexCount);
		}

		reset();
	}

	private void grow() {
		if(curInt + numIntsPerQuad > maxInts)
			flush();
	}

	public void vert(float x, float y, float z, int c, float u, float v) {
		int r = (c & 0x00ff0000) >> 16;
		int g = (c & 0x0000ff00) >> 8;
		int b = (c & 0x000000ff);
		int a = (c & 0xff000000) >>> 24;
		ints[curInt++] = Float.floatToRawIntBits(x);
		ints[curInt++] = Float.floatToRawIntBits(y);
		ints[curInt++] = Float.floatToRawIntBits(z);
		ints[curInt++] = a << 24 | b << 16 | g << 8 | r;
		//ints[curInt++] = c;
		ints[curInt++] = Float.floatToRawIntBits(u);
		ints[curInt++] = Float.floatToRawIntBits(v);
	}

	public void addRect(float x, float y, float z, float w, float h, int color, int alignX, int alignY) {
		if(!begun)
			return;

		if(alignX == Align.Center)
			x -= w / 2.0f;
		else if(alignX == Align.Right)
			x -= w;

		if(alignY == Align.Center)
			z -= h / 2.0f;
		else if(alignY == Align.Top)
			z -= h;

		float x1 = x;
		float y1 = y;
		float z1 = y;
		float x2 = x1 + w;
		float y2 = y1;
		float z2 = z1 + h;

		float u1 = 0;
		float v1 = 0;
		float u2 = 1;
		float v2 = 1;

		grow();
		vert(x1, y1, z1, color, u1, v1);
		vert(x2, y1, z1, color, u2, v1);
		vert(x2, y1, z2, color, u2, v2);
		vert(x1, y1, z2, color, u1, v2);
	}

	public void addRectXZ(float x1, float y, float z1, float x2, float z2, int c) {
	}

	public void addLine(float x1, float y1, float z1, float x2, float y2, float z2, int c1, int c2) {
		vert(x1, y1, z1, c1, 0, 0);
		vert(x2, y2, z2, c2, 0, 0);
	}

	public void addLine(float x1, float y1, float z1, float x2, float y2, float z2, int c) {
		addLine(x1, y1, z1, x2, y2, z2, c, c);
	}

	public void addAABB(float x1, float y1, float z1, float x2, float y2, float z2, int c) {
		addLine(x1, y2, z1, x1, y2, z2, c);
		addLine(x1, y2, z2, x2, y2, z2, c);
		addLine(x2, y2, z2, x2, y2, z1, c);
		addLine(x2, y2, z1, x1, y2, z1, c);

		addLine(x1, y2, z1, x1, y1, z1, c);
		addLine(x1, y2, z2, x1, y1, z2, c);
		addLine(x2, y2, z2, x2, y1, z2, c);
		addLine(x2, y2, z1, x2, y1, z1, c);

		addLine(x1, y1, z1, x1, y1, z2, c);
		addLine(x1, y1, z2, x2, y1, z2, c);
		addLine(x2, y1, z2, x2, y1, z1, c);
		addLine(x2, y1, z1, x1, y1, z1, c);
	}

	public void addPlayer(float x, float y, float z, float w, float h, float d, int color, float border) {
		float x1 = x - w / 2;
		float y1 = y;
		float z1 = z - d / 2;

		float x2 = x + w / 2;
		float y2 = y + h;
		float z2 = z + d / 2;

		x1 += border;
		y1 += border;
		z1 += border;

		x2 -= border;
		y2 -= border;
		z2 -= border;

		addAABB(x1, y1, z1, x2, y2, z2, color);
	}

	public void addPlayer(float x, float y, float z, float w, float h, float d, int color) {
		addPlayer(x, y, z, w, h, d, color, 0);
	}

	public void addPlayer(Vec3f position, Vec3f scale, int color, float border) {
		float x1 = position.x - scale.x / 2;
		float y1 = position.y;
		float z1 = position.z - scale.z / 2;

		float x2 = position.x + scale.x / 2;
		float y2 = position.y + scale.y;
		float z2 = position.z + scale.z / 2;

		x1 += border;
		y1 += border;
		z1 += border;

		x2 -= border;
		y2 -= border;
		z2 -= border;

		addAABB(x1, y1, z1, x2, y2, z2, color);
	}

	public static Draw3D instance;

	public static void createInstance() {
		if(instance == null) {
			instance = new Draw3D();
			instance.create();
		}
	}

	public static void destroyInstance() {
		if(instance != null) {
			instance.destroy();
			instance = null;
		}
	}
}