package com.indiedev101;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.stb.STBImage;

public class PNGImage {
	public static Image load(String path, int flags) {
		Image image = null;

		IntBuffer widthBuffer = BufferUtils.createIntBuffer(1);
		IntBuffer heightBuffer = BufferUtils.createIntBuffer(1);
		IntBuffer bppBuffer = BufferUtils.createIntBuffer(1);
		ByteBuffer dataBuffer = STBImage.stbi_load(path, widthBuffer, heightBuffer, bppBuffer, 4);
		if(dataBuffer != null) {
			int width = widthBuffer.get();
			int height = heightBuffer.get();
			int size = width * height * 4;
			byte[] data = new byte[width * height * 4];

			byte[] temp = new byte[size];
			dataBuffer.get(temp);

			if((flags & TextureFlags.NoPreMul) != 0) {
				for(int x = 0; x < width; x++) {
					for(int y = 0; y < height; y++) {
						int i = (y * width * 4) + (x * 4);
						int j = ((height - y - 1) * width * 4) + (x * 4);

						byte r = temp[i + 0];
						byte g = temp[i + 1];
						byte b = temp[i + 2];
						byte a = temp[i + 3];

						data[j + 0] = r;
						data[j + 1] = g;
						data[j + 2] = b;
						data[j + 3] = a;
					}
				}
			} else {
				for(int x = 0; x < width; x++) {
					for(int y = 0; y < height; y++) {
						int i = (y * width * 4) + (x * 4);
						int j = ((height - y - 1) * width * 4) + (x * 4);

						float r = (float)(temp[i + 0] & 255);
						float g = (float)(temp[i + 1] & 255);
						float b = (float)(temp[i + 2] & 255);
						float a = (float)(temp[i + 3] & 255);

						float t = a / 255.0f;

						data[j + 0] = (byte)(int)(r * t);
						data[j + 1] = (byte)(int)(g * t);
						data[j + 2] = (byte)(int)(b * t);
						data[j + 3] = (byte)(int)(a);
					}
				}
			}

			image = new Image();
			image.width = width;
			image.height = height;
			image.bpp = 4;
			image.size = size;
			image.data = data;
		} else {
			Log.error("Failed to open %s\n", path);
		}
		if(image != null) {
			image.dataAsByteBuffer = BufferUtils.createByteBuffer(image.size);
			image.dataAsByteBuffer.put(image.data);
			image.dataAsByteBuffer.position(0);
			image.dataAsByteBuffer.limit(image.size);
		}
		return image;
	}

	public static Image load(String path) {
		return load(path, 0);
	}
}