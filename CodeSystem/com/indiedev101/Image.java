package com.indiedev101;

import java.nio.ByteBuffer;

public class Image {
	public int width;
	public int height;
	public int bpp;
	public int size;
	public byte[] data;
	public ByteBuffer dataAsByteBuffer;

	public static void swap_BGR_RGB(int size, byte[] data) {
		for(int i = 0; i < size; i += 3) {
			byte b = data[i + 0];
			byte g = data[i + 1];
			byte r = data[i + 2];
			data[i + 0] = r;
			data[i + 1] = g;
			data[i + 2] = b;
		}
	}

	public static void swap_BGRA_RGBA(int size, byte[] data) {
		for(int i = 0; i < size; i += 4) {
			byte b = data[i + 0];
			byte g = data[i + 1];
			byte r = data[i + 2];
			byte a = data[i + 3];
			data[i + 0] = r;
			data[i + 1] = g;
			data[i + 2] = b;
			data[i + 3] = a;
		}
	}

	public static void swap_ABGR_RGBA(int size, byte[] data) {
		for(int i = 0; i < size; i += 4) {
			byte a = data[i + 0];
			byte b = data[i + 1];
			byte g = data[i + 2];
			byte r = data[i + 3];
			data[i + 0] = r;
			data[i + 1] = g;
			data[i + 2] = b;
			data[i + 3] = a;
		}
	}

	public static byte[] flip_BGR_RGB(int width, int height, byte[] data) {
		byte[] temp = new byte[width * height * 3];
		for(int x = 0; x < width; x++) {
			for(int y = 0; y < height; y++) {
				int i = (y * width * 3) + (x * 3);
				int j = ((height - y - 1) * width * 3) + (x * 3);
				byte b = data[i + 0];
				byte g = data[i + 1];
				byte r = data[i + 2];
				temp[j + 0] = r;
				temp[j + 1] = g;
				temp[j + 2] = b;
			}
		}
		return temp;
	}

	public static byte[] flip_ABGR_RGBA(int width, int height, byte[] data) {
		byte[] temp = new byte[width * height * 4];
		for(int x = 0; x < width; x++) {
			for(int y = 0; y < height; y++) {
				int i = (y * width * 4) + (x * 4);
				int j = ((height - y - 1) * width * 4) + (x * 4);
				byte a = data[i + 0];
				byte b = data[i + 1];
				byte g = data[i + 2];
				byte r = data[i + 3];
				temp[j + 0] = r;
				temp[j + 1] = g;
				temp[j + 2] = b;
				temp[j + 3] = a;
			}
		}
		return temp;
	}

	public static Image createImage(String path, int flags) {
		Image image = null;
		if(path.endsWith(".tga"))
			image = TGAImage.load(path);
		else if(path.endsWith(".png"))
			image = PNGImage.load(path, flags);
		return image;
	}

	public static Image createImage(String path) {
		return createImage(path, 0);
	}
}