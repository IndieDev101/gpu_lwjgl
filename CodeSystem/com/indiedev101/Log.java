package com.indiedev101;

public class Log {
	private static Object lock = new Object();

	private static void out(String str) {
		synchronized(lock) {
			System.out.print(str);
		}
	}

	private static void err(String str) {
		synchronized(lock) {
			System.err.print(str);
		}
	}

	public static void fatal(String arg0, Object... arg1) {
		String str1 = String.format(arg0, arg1);
		String str2 = String.format("FATAL: %s", str1);
		err(str2);
		throw new RuntimeException("FATAL");
	}

	public static void error(String arg0, Object... arg1) {
		String str1 = String.format(arg0, arg1);
		String str2 = String.format("ERROR: %s", str1);
		err(str2);
	}

	public static void warn(String arg0, Object... arg1) {
		String str1 = String.format(arg0, arg1);
		String str2 = String.format("WARNING: %s", str1);
		out(str2);
	}

	public static void debug(String arg0, Object... arg1) {
		String str = String.format(arg0, arg1);
		out(str);
	}

	public static void tabs(int tabs, String arg0, Object... arg1) {
		String str = "";
		for(int i = 0; i < tabs; i++)
			str += "   ";
		str += String.format(arg0, arg1);
		out(str);
	}
}