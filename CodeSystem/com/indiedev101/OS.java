package com.indiedev101;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class OS {
	private static final int UNKNOWN = 0;
	private static final int WIN = 1;
	private static final int MAC = 2;
	private static final int DESKTOP = 1;

	private static final int getPlatform() {
		String platformName = System.getProperty("os.name").toLowerCase();
		Log.debug("Platform: '%s'\n", platformName);
		if(platformName.contains("win"))
			return WIN;
		else if(platformName.contains("mac"))
			return MAC;
		else
			return UNKNOWN;
	}

	private static final int platform = getPlatform();
	private static final int computer = getComputer();

	public static final boolean isWIN() {
		return platform == WIN;
	}

	public static final boolean isMAC() {
		return platform == MAC;
	}

	private static final int getComputer() {
		try {
			String computerName = InetAddress.getLocalHost().getHostName();
			Log.debug("Computer: '%s'\n", computerName);
			if(computerName.equals("DESKTOP"))
				return DESKTOP;
		} catch(UnknownHostException e) {
			e.printStackTrace();
		}
		return UNKNOWN;
	}

	public static final boolean isDESKTOP() {
		return computer == DESKTOP;
	}
}