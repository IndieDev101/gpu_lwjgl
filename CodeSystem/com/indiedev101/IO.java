package com.indiedev101;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class IO {
	public static String getWorkingDir() {
		if(OS.isMAC())
			return "/Users/lloyd/tutjava/Data/com/indiedev101";
		else
			return "C:/tutjava/Data/com/indiedev101";
	}

	public static String getFilePath(String name) {
		return String.format("%s/%s", getWorkingDir(), name);
	}

	public static long getFileTime(String path) {
		File file = new File(path);
		long time = file.lastModified();
		return time;
	}

	public static byte[] getFileAsBytes(String path) {
		byte[] bytes = null;
		FileInputStream fileInputStream = null;
		try {
			File file = new File(path);
			fileInputStream = new FileInputStream(file);
			long size = file.length();
			if(size != 0L) {
				bytes = new byte[(int)size];
				fileInputStream.read(bytes);
			}
		} catch(FileNotFoundException e) {
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if(fileInputStream != null) {
				try {
					fileInputStream.close();
				} catch(IOException e) {
					e.printStackTrace();
				}
			}
		}
		return bytes;
	}

	public static String getFileAsString(String path) {
		String string = null;
		byte[] bytes = getFileAsBytes(path);
		if(bytes != null) {
			try {
				string = new String(bytes, "UTF-8");
			} catch(UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		return string;
	}

	public static String[] getFileAsLines(String path) {
		String[] lines = null;
		String string = getFileAsString(path);
		if(string != null)
			lines = string.split("[\r\n]+");
		return lines;
	}
}