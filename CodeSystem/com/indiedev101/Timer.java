package com.indiedev101;

import org.lwjgl.glfw.GLFW;

public class Timer {
	private static long oldTicks;
	private static long updates;

	public static float timeStep;

	public static void update() {
		long newTicks = System.nanoTime();
		if(updates == 0)
			oldTicks = newTicks;
		long ticks = newTicks - oldTicks;
		oldTicks = newTicks;
		updates++;
		timeStep = TICKStoS(ticks);
	}

	public static double getTime() {
		return GLFW.glfwGetTime();
	}

	public static long getTicks() {
		return System.nanoTime();
	}

	public static float TICKStoS(long ticks) {
		return (float)((double)ticks / 1000000000.0);
	}

	public static float TICKStoMS(long ticks) {
		return (float)((double)ticks / 1000000.0);
	}

	public static long StoTICKS(float s) {
		return (long)((double)s * 1000000000.0);
	}

	public static double MStoTICKS(float ms) {
		return (long)((double)ms * 1000000.0);
	}
}