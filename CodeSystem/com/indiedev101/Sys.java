package com.indiedev101;

public class Sys {
	public static final boolean debug = true;

	public static void sleep(long ms) {
		try {
			Thread.sleep(ms);
		} catch(InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static Object classForName(String name) {
		try {
			return Class.forName("com.indiedev101." + name).newInstance();
		} catch(InstantiationException e) {
			e.printStackTrace();
		} catch(IllegalAccessException e) {
			e.printStackTrace();
		} catch(ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
}