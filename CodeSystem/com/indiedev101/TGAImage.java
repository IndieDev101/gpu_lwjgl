package com.indiedev101;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.lwjgl.BufferUtils;

public class TGAImage {
	public static Image load(String path, int flags) {
		Image image = null;

		FileInputStream fis = null;
		DataInputStream dis = null;
		try {
			File file = new File(path);
			fis = new FileInputStream(file);
			BufferedInputStream bis = new BufferedInputStream(fis, 1024 * 1024);
			dis = new DataInputStream(bis);

			int idLength = dis.read();
			int colorMapType = dis.read();
			int tgaType = dis.read();
			int cMapStart = endianSwap(dis.readShort());
			int cMapLength = endianSwap(dis.readShort());
			int cMapDepth = dis.read();
			int xOffset = endianSwap(dis.readShort());
			int yOffset = endianSwap(dis.readShort());
			int width = endianSwap(dis.readShort());
			int height = endianSwap(dis.readShort());
			int bpp = dis.read() / 8;
			int imageDescriptor = dis.read();
			if(idLength > 0)
				dis.skip(idLength);

			int size = width * height * bpp;
			byte[] data = new byte[size];
			dis.read(data, 0, size);

			image = new Image();
			image.width = width;
			image.height = height;
			image.bpp = bpp;
			image.size = size;
			image.data = data;
		} catch(FileNotFoundException e) {
			Log.error("Failed to open %s\n", path);
		} catch(IOException e) {
			e.printStackTrace();
		}
		if(dis != null) {
			try {
				dis.close();
			} catch(IOException e) {
				e.printStackTrace();
			}
		}
		if(fis != null) {
			try {
				fis.close();
			} catch(IOException e) {
				e.printStackTrace();
			}
		}

		if(image != null && (flags & ImageFlags.Raw) == 0) {
			if(image.bpp == 3)
				Image.swap_BGR_RGB(image.size, image.data);
			else if(image.bpp == 4)
				Image.swap_BGRA_RGBA(image.size, image.data);
		}
		if(image != null) {
			image.dataAsByteBuffer = BufferUtils.createByteBuffer(image.size);
			image.dataAsByteBuffer.put(image.data);
			image.dataAsByteBuffer.position(0);
			image.dataAsByteBuffer.limit(image.size);
		}
		return image;
	}

	public static Image load(String path) {
		return load(path, 0);
	}

	private static short endianSwap(short signedShort) {
		int shortAsInt = signedShort & 0xffff;
		return (short)(shortAsInt << 8 | (shortAsInt & 0xff00) >>> 8);
	}
}
