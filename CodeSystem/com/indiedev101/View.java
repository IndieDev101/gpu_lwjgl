package com.indiedev101;

import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;

public class View {
	public float fieldOfView;
	public float aspectRatio;
	public float nearClip;
	public float farClip;

	public Vec3f pos = new Vec3f();
	public Vec3f dir = new Vec3f();
	public Vec3f up = new Vec3f();

	public Mat4f screenMatrix = new Mat4f();
	public Mat4f worldMatrix = new Mat4f();
	public Mat4f viewMatrix = new Mat4f();
	public Mat4f projMatrix = new Mat4f();

	public int screenMatrixDirty;
	public int worldMatrixDirty;
	public int viewMatrixDirty;
	public int projMatrixDirty;

	private FloatBuffer screenMatrixBuffer = BufferUtils.createFloatBuffer(16);
	private FloatBuffer worldMatrixBuffer = BufferUtils.createFloatBuffer(16);
	private FloatBuffer viewMatrixBuffer = BufferUtils.createFloatBuffer(16);
	private FloatBuffer projMatrixBuffer = BufferUtils.createFloatBuffer(16);

	private int screenMatrixBufferDirty;
	private int worldMatrixBufferDirty;
	private int viewMatrixBufferDirty;
	private int projMatrixBufferDirty;

	public View() {
		screenMatrix.setIdentity();
		worldMatrix.setIdentity();
		viewMatrix.setIdentity();
		projMatrix.setIdentity();

		MathUtils.putMatrixInBuffer(screenMatrix, screenMatrixBuffer);
		MathUtils.putMatrixInBuffer(worldMatrix, worldMatrixBuffer);
		MathUtils.putMatrixInBuffer(viewMatrix, viewMatrixBuffer);
		MathUtils.putMatrixInBuffer(projMatrix, projMatrixBuffer);
	}

	public FloatBuffer getScreenMatrixBuffer() {
		if(screenMatrixBufferDirty < screenMatrixDirty) {
			MathUtils.putMatrixInBuffer(screenMatrix, screenMatrixBuffer);
			screenMatrixBufferDirty = screenMatrixDirty;
		}
		return screenMatrixBuffer;
	}

	public FloatBuffer getWorldMatrixBuffer() {
		if(worldMatrixBufferDirty < worldMatrixDirty) {
			MathUtils.putMatrixInBuffer(worldMatrix, worldMatrixBuffer);
			worldMatrixBufferDirty = worldMatrixDirty;
		}
		return worldMatrixBuffer;
	}

	public FloatBuffer getViewMatrixBuffer() {
		if(viewMatrixBufferDirty < viewMatrixDirty) {
			MathUtils.putMatrixInBuffer(viewMatrix, viewMatrixBuffer);
			viewMatrixBufferDirty = viewMatrixDirty;
		}
		return viewMatrixBuffer;
	}

	public FloatBuffer getProjMatrixBuffer() {
		if(projMatrixBufferDirty < projMatrixDirty) {
			MathUtils.putMatrixInBuffer(projMatrix, projMatrixBuffer);
			projMatrixBufferDirty = projMatrixDirty;
		}
		return projMatrixBuffer;
	}

	public void set2D(int width, int height, float nearClip, float farClip) {
		MathUtils.setOrtho(width, height, nearClip, farClip, screenMatrix);
		screenMatrixDirty++;
	}

	public void set3D(float fieldOfView, float aspectRatio, float nearClip, float farClip, Vec3f pos, Vec3f dir, Vec3f up, Mat4f viewMatrix, Mat4f projMatrix) {
		this.pos.set(pos);
		this.dir.set(dir);
		this.up.set(up);
		this.fieldOfView = fieldOfView;
		this.aspectRatio = aspectRatio;
		this.nearClip = nearClip;
		this.farClip = farClip;
		this.worldMatrix.setIdentity();
		this.viewMatrix.set(viewMatrix);
		this.projMatrix.set(projMatrix);
		worldMatrixDirty++;
		viewMatrixDirty++;
		projMatrixDirty++;
	}

	public static View instance;

	public static void createInstance() {
		if(instance == null)
			instance = new View();
	}

	public static void destroyInstance() {
		if(instance != null)
			instance = null;
	}
}