package com.indiedev101;

import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFW;

public class App {
	public static boolean quit;
	public static int width;
	public static int height;
	public static long window;
	public static boolean focus;
	
	public static void resize() {
		IntBuffer w = BufferUtils.createIntBuffer(1);
		IntBuffer h = BufferUtils.createIntBuffer(1);
		GLFW.glfwGetWindowSize(window, w, h);
		width = w.get();
		height = h.get();
	}

	public static int win2gpuX(double x) {
		return (int)((x / (double)width) * (double)GPU.width);
	}

	public static int win2gpuY(double y) {
		return (int)((((double)height - y) / (double)height) * (double)GPU.height);
	}

	public static int win2gpuX(float x) {
		return win2gpuX((double)x);
	}

	public static int win2gpuY(float y) {
		return win2gpuY((double)y);
	}

	public static int win2gpuX(int x) {
		return win2gpuX((double)x);
	}

	public static int win2gpuY(int y) {
		return win2gpuY((double)y);
	}

	public static int gpu2winX(double x) {
		return (int)((x / (double)GPU.width) * (double)width);
	}

	public static int gpu2winY(double y) {
		return height - (int)((y / (double)GPU.height) * (double)height);
	}

	public static int gpu2winX(float x) {
		return gpu2winX((double)x);
	}

	public static int gpu2winY(float y) {
		return gpu2winY((double)y);
	}

	public static int gpu2winX(int x) {
		return gpu2winX((double)x);
	}

	public static int gpu2winY(int y) {
		return gpu2winY((double)y);
	}
}