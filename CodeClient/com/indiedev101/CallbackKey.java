package com.indiedev101;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWKeyCallback;

public class CallbackKey extends GLFWKeyCallback {
	@Override
	public void invoke(long window, int key, int scancode, int action, int mods) {
		//Log.debug("CallbackKey: %d %d %d %d %d %s\n", window, key, scancode, action, mods, GLFW.glfwGetKeyName(key, scancode));
		if(key < 0 || key >= Keyboard.maxKeys)
			return;

		if(action == GLFW.GLFW_PRESS) {
			Keyboard.currKey[key] = true;

			if(key == GLFW.GLFW_KEY_ESCAPE) {
				if(Mouse.isGrabbed()) {
					Mouse.setGrabbed(false);
				} else {
					GLFW.glfwSetWindowShouldClose(window, true);
				}
			}

			Client.instance.keyDown(key);
		} else if(action == GLFW.GLFW_RELEASE) {
			Keyboard.currKey[key] = false;

			Client.instance.keyUp(key);
		}
	}
}