package com.indiedev101;

import org.lwjgl.glfw.GLFWWindowSizeCallback;

public class CallbackWindowSize extends GLFWWindowSizeCallback {
	@Override
	public void invoke(long window, int width, int height) {
		//Log.debug("CallbackWindowSize: %d %d %d\n", window, width, height);

		App.resize();
	}
}