package com.indiedev101;

import org.lwjgl.glfw.GLFWWindowFocusCallback;

public class CallbackWindowFocus extends GLFWWindowFocusCallback {
	@Override
	public void invoke(long window, boolean focused) {
		//Log.debug("CallbackWindowFocus: %d %s\n", window, focused);

		if(!focused && Mouse.isGrabbed())
			Mouse.setGrabbed(false);

		App.focus = focused;
	}
}