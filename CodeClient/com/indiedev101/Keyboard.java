package com.indiedev101;

import org.lwjgl.glfw.GLFW;

public class Keyboard {
	public static final int maxKeys = GLFW.GLFW_KEY_LAST + 1;
	public static boolean[] currKey = new boolean[maxKeys];
	public static boolean[] prevKey = new boolean[maxKeys];

	public static void prePollEvents() {
		for(int i = 0; i < maxKeys; i++)
			prevKey[i] = currKey[i];
	}

	public static boolean isKeyDown(int key) {
		return currKey[key];
	}

	public static boolean isKeyPress(int key) {
		return currKey[key] && !prevKey[key];
	}

	public static final int KeySpace = GLFW.GLFW_KEY_SPACE;
	public static final int KeyApostrophe = GLFW.GLFW_KEY_APOSTROPHE;
	public static final int KeyComma = GLFW.GLFW_KEY_COMMA;
	public static final int KeyMinus = GLFW.GLFW_KEY_MINUS;
	public static final int KeyPeriod = GLFW.GLFW_KEY_PERIOD;
	public static final int KeySlash = GLFW.GLFW_KEY_SLASH;
	public static final int Key0 = GLFW.GLFW_KEY_0;
	public static final int Key1 = GLFW.GLFW_KEY_1;
	public static final int Key2 = GLFW.GLFW_KEY_2;
	public static final int Key3 = GLFW.GLFW_KEY_3;
	public static final int Key4 = GLFW.GLFW_KEY_4;
	public static final int Key5 = GLFW.GLFW_KEY_5;
	public static final int Key6 = GLFW.GLFW_KEY_6;
	public static final int Key7 = GLFW.GLFW_KEY_7;
	public static final int Key8 = GLFW.GLFW_KEY_8;
	public static final int Key9 = GLFW.GLFW_KEY_9;
	public static final int KeySemicolon = GLFW.GLFW_KEY_SEMICOLON;
	public static final int KeyEqual = GLFW.GLFW_KEY_EQUAL;
	public static final int KeyA = GLFW.GLFW_KEY_A;
	public static final int KeyB = GLFW.GLFW_KEY_B;
	public static final int KeyC = GLFW.GLFW_KEY_C;
	public static final int KeyD = GLFW.GLFW_KEY_D;
	public static final int KeyE = GLFW.GLFW_KEY_E;
	public static final int KeyF = GLFW.GLFW_KEY_F;
	public static final int KeyG = GLFW.GLFW_KEY_G;
	public static final int KeyH = GLFW.GLFW_KEY_H;
	public static final int KeyI = GLFW.GLFW_KEY_I;
	public static final int KeyJ = GLFW.GLFW_KEY_J;
	public static final int KeyK = GLFW.GLFW_KEY_K;
	public static final int KeyL = GLFW.GLFW_KEY_L;
	public static final int KeyM = GLFW.GLFW_KEY_M;
	public static final int KeyN = GLFW.GLFW_KEY_N;
	public static final int KeyO = GLFW.GLFW_KEY_O;
	public static final int KeyP = GLFW.GLFW_KEY_P;
	public static final int KeyQ = GLFW.GLFW_KEY_Q;
	public static final int KeyR = GLFW.GLFW_KEY_R;
	public static final int KeyS = GLFW.GLFW_KEY_S;
	public static final int KeyT = GLFW.GLFW_KEY_T;
	public static final int KeyU = GLFW.GLFW_KEY_U;
	public static final int KeyV = GLFW.GLFW_KEY_V;
	public static final int KeyW = GLFW.GLFW_KEY_W;
	public static final int KeyX = GLFW.GLFW_KEY_X;
	public static final int KeyY = GLFW.GLFW_KEY_Y;
	public static final int KeyZ = GLFW.GLFW_KEY_Z;
	public static final int KeyLeftBracket = GLFW.GLFW_KEY_LEFT_BRACKET;
	public static final int KeyBackslash = GLFW.GLFW_KEY_BACKSLASH;
	public static final int KeyRightBracket = GLFW.GLFW_KEY_RIGHT_BRACKET;
	public static final int KeyGraveAccent = GLFW.GLFW_KEY_GRAVE_ACCENT;
	public static final int KeyWorld1 = GLFW.GLFW_KEY_WORLD_1;
	public static final int KeyWorld2 = GLFW.GLFW_KEY_WORLD_2;
	public static final int KeyEscape = GLFW.GLFW_KEY_ESCAPE;
	public static final int KeyEnter = GLFW.GLFW_KEY_ENTER;
	public static final int KeyTab = GLFW.GLFW_KEY_TAB;
	public static final int KeyBackspace = GLFW.GLFW_KEY_BACKSPACE;
	public static final int KeyInsert = GLFW.GLFW_KEY_INSERT;
	public static final int KeyDelete = GLFW.GLFW_KEY_DELETE;
	public static final int KeyRight = GLFW.GLFW_KEY_RIGHT;
	public static final int KeyLeft = GLFW.GLFW_KEY_LEFT;
	public static final int KeyDown = GLFW.GLFW_KEY_DOWN;
	public static final int KeyUp = GLFW.GLFW_KEY_UP;
	public static final int KeyPageUp = GLFW.GLFW_KEY_PAGE_UP;
	public static final int KeyPageDown = GLFW.GLFW_KEY_PAGE_DOWN;
	public static final int KeyHome = GLFW.GLFW_KEY_HOME;
	public static final int KeyEnd = GLFW.GLFW_KEY_END;
	public static final int KeyCapsLock = GLFW.GLFW_KEY_CAPS_LOCK;
	public static final int KeyScrollLock = GLFW.GLFW_KEY_SCROLL_LOCK;
	public static final int KeyNumLock = GLFW.GLFW_KEY_NUM_LOCK;
	public static final int KeyPrintScreen = GLFW.GLFW_KEY_PRINT_SCREEN;
	public static final int KeyPause = GLFW.GLFW_KEY_PAUSE;
	public static final int KeyF1 = GLFW.GLFW_KEY_F1;
	public static final int KeyF2 = GLFW.GLFW_KEY_F2;
	public static final int KeyF3 = GLFW.GLFW_KEY_F3;
	public static final int KeyF4 = GLFW.GLFW_KEY_F4;
	public static final int KeyF5 = GLFW.GLFW_KEY_F5;
	public static final int KeyF6 = GLFW.GLFW_KEY_F6;
	public static final int KeyF7 = GLFW.GLFW_KEY_F7;
	public static final int KeyF8 = GLFW.GLFW_KEY_F8;
	public static final int KeyF9 = GLFW.GLFW_KEY_F9;
	public static final int KeyF10 = GLFW.GLFW_KEY_F10;
	public static final int KeyF11 = GLFW.GLFW_KEY_F11;
	public static final int KeyF12 = GLFW.GLFW_KEY_F12;
	public static final int KeyF13 = GLFW.GLFW_KEY_F13;
	public static final int KeyF14 = GLFW.GLFW_KEY_F14;
	public static final int KeyF15 = GLFW.GLFW_KEY_F15;
	public static final int KeyF16 = GLFW.GLFW_KEY_F16;
	public static final int KeyF17 = GLFW.GLFW_KEY_F17;
	public static final int KeyF18 = GLFW.GLFW_KEY_F18;
	public static final int KeyF19 = GLFW.GLFW_KEY_F19;
	public static final int KeyF20 = GLFW.GLFW_KEY_F20;
	public static final int KeyF21 = GLFW.GLFW_KEY_F21;
	public static final int KeyF22 = GLFW.GLFW_KEY_F22;
	public static final int KeyF23 = GLFW.GLFW_KEY_F23;
	public static final int KeyF24 = GLFW.GLFW_KEY_F24;
	public static final int KeyF25 = GLFW.GLFW_KEY_F25;
	public static final int KeyPad0 = GLFW.GLFW_KEY_KP_0;
	public static final int KeyPad1 = GLFW.GLFW_KEY_KP_1;
	public static final int KeyPad2 = GLFW.GLFW_KEY_KP_2;
	public static final int KeyPad3 = GLFW.GLFW_KEY_KP_3;
	public static final int KeyPad4 = GLFW.GLFW_KEY_KP_4;
	public static final int KeyPad5 = GLFW.GLFW_KEY_KP_5;
	public static final int KeyPad6 = GLFW.GLFW_KEY_KP_6;
	public static final int KeyPad7 = GLFW.GLFW_KEY_KP_7;
	public static final int KeyPad8 = GLFW.GLFW_KEY_KP_8;
	public static final int KeyPad9 = GLFW.GLFW_KEY_KP_9;
	public static final int KeyPadDecimal = GLFW.GLFW_KEY_KP_DECIMAL;
	public static final int KeyPadDivide = GLFW.GLFW_KEY_KP_DIVIDE;
	public static final int KeyPadMultiply = GLFW.GLFW_KEY_KP_MULTIPLY;
	public static final int KeyPadSubtract = GLFW.GLFW_KEY_KP_SUBTRACT;
	public static final int KeyPadAdd = GLFW.GLFW_KEY_KP_ADD;
	public static final int KeyPadEnter = GLFW.GLFW_KEY_KP_ENTER;
	public static final int KeyPadEqual = GLFW.GLFW_KEY_KP_EQUAL;
	public static final int KeyLeftShift = GLFW.GLFW_KEY_LEFT_SHIFT;
	public static final int KeyLeftControl = GLFW.GLFW_KEY_LEFT_CONTROL;
	public static final int KeyLeftAlt = GLFW.GLFW_KEY_LEFT_ALT;
	public static final int KeyLeftSuper = GLFW.GLFW_KEY_LEFT_SUPER;
	public static final int KeyRightShift = GLFW.GLFW_KEY_RIGHT_SHIFT;
	public static final int KeyRightControl = GLFW.GLFW_KEY_RIGHT_CONTROL;
	public static final int KeyRightAlt = GLFW.GLFW_KEY_RIGHT_ALT;
	public static final int KeyRightSuper = GLFW.GLFW_KEY_RIGHT_SUPER;
	public static final int KeyMenu = GLFW.GLFW_KEY_MENU;
	public static final int KeyTilde = GLFW.GLFW_KEY_GRAVE_ACCENT;
}