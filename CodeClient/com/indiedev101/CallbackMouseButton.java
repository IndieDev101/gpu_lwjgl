package com.indiedev101;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWMouseButtonCallback;

public class CallbackMouseButton extends GLFWMouseButtonCallback {
	@Override
	public void invoke(long window, int button, int action, int mods) {
		//Log.debug("CallbackMouseButton: %d %d %d %d\n", window, button, action, mods);
		if(button < 0 || button >= Mouse.maxButtons)
			return;

		if(action == GLFW.GLFW_PRESS) {
			Mouse.currButton[button] = true;

			if(!Client.instance.mouseDown(button, Mouse.currX, Mouse.currY)) {
				if(button == 0 && !Mouse.isGrabbed())
					Mouse.setGrabbed(true);
			}
		} else if(action == GLFW.GLFW_RELEASE) {
			Mouse.currButton[button] = false;

			Client.instance.mouseUp(button, Mouse.currX, Mouse.currY);
		}
	}
}