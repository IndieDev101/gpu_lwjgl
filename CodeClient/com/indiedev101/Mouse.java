package com.indiedev101;

import org.lwjgl.glfw.GLFW;

public class Mouse {
	public static final int maxButtons = GLFW.GLFW_MOUSE_BUTTON_LAST + 1;
	public static boolean[] currButton = new boolean[maxButtons];
	public static boolean[] prevButton = new boolean[maxButtons];

	public static int prevX;
	public static int prevY;
	public static int currX;
	public static int currY;
	public static int moveX;
	public static int moveY;
	public static int wheel;

	public static boolean canGrab = true;
	public static boolean bGrabbed;
	public static int iGrabbed;

	public static void prePollEvents() {
		for(int i = 0; i < maxButtons; i++)
			prevButton[i] = currButton[i];

		prevX = currX;
		prevY = currY;
		moveX = 0;
		moveY = 0;
		wheel = 0;
	}

	public static boolean isButtonDown(int button) {
		return currButton[button];
	}

	public static boolean isButtonPress(int button) {
		return currButton[button] && !prevButton[button];
	}

	public static boolean isGrabbed() {
		return bGrabbed;
	}

	public static void setGrabbed(boolean grab) {
		if(grab) {
			if(!bGrabbed && canGrab) {
				GLFW.glfwSetInputMode(App.window, GLFW.GLFW_CURSOR, GLFW.GLFW_CURSOR_DISABLED);
				bGrabbed = true;
				iGrabbed = 0;
			}
		} else {
			if(bGrabbed) {
				GLFW.glfwSetInputMode(App.window, GLFW.GLFW_CURSOR, GLFW.GLFW_CURSOR_NORMAL);
				bGrabbed = false;
			}
		}
	}

	public static final int Button0 = GLFW.GLFW_MOUSE_BUTTON_1;
	public static final int Button1 = GLFW.GLFW_MOUSE_BUTTON_2;
	public static final int Button2 = GLFW.GLFW_MOUSE_BUTTON_3;
	public static final int Button3 = GLFW.GLFW_MOUSE_BUTTON_4;
	public static final int Button4 = GLFW.GLFW_MOUSE_BUTTON_5;
	public static final int Button5 = GLFW.GLFW_MOUSE_BUTTON_6;
	public static final int Button6 = GLFW.GLFW_MOUSE_BUTTON_7;
	public static final int Button7 = GLFW.GLFW_MOUSE_BUTTON_8;
}