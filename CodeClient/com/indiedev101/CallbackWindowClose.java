package com.indiedev101;

import org.lwjgl.glfw.GLFWWindowCloseCallback;

public class CallbackWindowClose extends GLFWWindowCloseCallback {
	@Override
	public void invoke(long window) {
		Log.debug("CallbackWindowClose: %d\n", window);
	}
}