package com.indiedev101;

import org.lwjgl.glfw.GLFWCursorEnterCallback;

public class CallbackCursorEnter extends GLFWCursorEnterCallback {
	@Override
	public void invoke(long window, boolean entered) {
		//Log.debug("CallbackCursorEnter: %d %s\n", window, entered);
	}
}