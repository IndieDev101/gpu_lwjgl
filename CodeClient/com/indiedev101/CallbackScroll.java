package com.indiedev101;

import org.lwjgl.glfw.GLFWScrollCallback;

public class CallbackScroll extends GLFWScrollCallback {
	@Override
	public void invoke(long window, double xoffset, double yoffset) {
		//Log.debug("CallbackScroll: %d %f %f\n", window, xoffset, yoffset);

		if(yoffset > 0)
			Mouse.wheel += 1;
		else if(yoffset < 0)
			Mouse.wheel -= 1;
	}
}