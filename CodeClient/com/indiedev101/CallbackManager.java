package com.indiedev101;

import org.lwjgl.glfw.GLFW;

public class CallbackManager {
	public static CallbackChar cbChar;
	public static CallbackCharMods cbCharMods;
	public static CallbackCursorEnter cbCursorEnter;
	public static CallbackCursorPos cbCursorPos;
	public static CallbackDrop cbDrop;
	public static CallbackError cbError;
	public static CallbackFramebufferSize cbFramebufferSize;
	public static CallbackJoystick cbJoystick;
	public static CallbackKey cbKey;
	public static CallbackMonitor cbMonitor;
	public static CallbackMouseButton cbMouseButton;
	public static CallbackScroll cbScroll;
	public static CallbackWindowClose cbWindowClose;
	public static CallbackWindowFocus cbWindowFocus;
	public static CallbackWindowIconify cbWindowIconify;
	public static CallbackWindowPos cbWindowPos;
	public static CallbackWindowSize cbWindowSize;
	public static CallbackWindowRefresh cbWindowRefresh;

	public static void preInit() {
		cbError = new CallbackError();

		GLFW.glfwSetErrorCallback(cbError);
	}

	public static void postInit() {
		cbJoystick = new CallbackJoystick();
		cbMonitor = new CallbackMonitor();

		GLFW.glfwSetJoystickCallback(cbJoystick);
		GLFW.glfwSetMonitorCallback(cbMonitor);
	}

	public static void postCreateWindow(long window) {
		cbChar = new CallbackChar();
		cbCharMods = new CallbackCharMods();
		cbCursorEnter = new CallbackCursorEnter();
		cbCursorPos = new CallbackCursorPos();
		cbDrop = new CallbackDrop();
		cbFramebufferSize = new CallbackFramebufferSize();
		cbKey = new CallbackKey();
		cbMouseButton = new CallbackMouseButton();
		cbScroll = new CallbackScroll();
		cbWindowClose = new CallbackWindowClose();
		cbWindowFocus = new CallbackWindowFocus();
		cbWindowIconify = new CallbackWindowIconify();
		cbWindowPos = new CallbackWindowPos();
		cbWindowRefresh = new CallbackWindowRefresh();
		cbWindowSize = new CallbackWindowSize();

		GLFW.glfwSetCharCallback(window, cbChar);
		GLFW.glfwSetCharModsCallback(window, cbCharMods);
		GLFW.glfwSetCursorEnterCallback(window, cbCursorEnter);
		GLFW.glfwSetCursorPosCallback(window, cbCursorPos);
		GLFW.glfwSetDropCallback(window, cbDrop);
		GLFW.glfwSetFramebufferSizeCallback(window, cbFramebufferSize);
		GLFW.glfwSetKeyCallback(window, cbKey);
		GLFW.glfwSetMouseButtonCallback(window, cbMouseButton);
		GLFW.glfwSetScrollCallback(window, cbScroll);
		GLFW.glfwSetWindowCloseCallback(window, cbWindowClose);
		GLFW.glfwSetWindowFocusCallback(window, cbWindowFocus);
		GLFW.glfwSetWindowIconifyCallback(window, cbWindowIconify);
		GLFW.glfwSetWindowPosCallback(window, cbWindowPos);
		GLFW.glfwSetWindowRefreshCallback(window, cbWindowRefresh);
		GLFW.glfwSetWindowSizeCallback(window, cbWindowSize);
	}

	public static void preDestroyWindow(long window) {
		GLFW.glfwSetCharCallback(window, null).free();
		GLFW.glfwSetCharModsCallback(window, null).free();
		GLFW.glfwSetCursorEnterCallback(window, null).free();
		GLFW.glfwSetCursorPosCallback(window, null).free();
		GLFW.glfwSetDropCallback(window, null).free();
		GLFW.glfwSetFramebufferSizeCallback(window, null).free();
		GLFW.glfwSetKeyCallback(window, null).free();
		GLFW.glfwSetMouseButtonCallback(window, null).free();
		GLFW.glfwSetScrollCallback(window, null).free();
		GLFW.glfwSetWindowCloseCallback(window, null).free();
		GLFW.glfwSetWindowFocusCallback(window, null).free();
		GLFW.glfwSetWindowIconifyCallback(window, null).free();
		GLFW.glfwSetWindowPosCallback(window, null).free();
		GLFW.glfwSetWindowRefreshCallback(window, null).free();
		GLFW.glfwSetWindowSizeCallback(window, null).free();

		cbChar = null;
		cbCharMods = null;
		cbCursorEnter = null;
		cbCursorPos = null;
		cbDrop = null;
		cbFramebufferSize = null;
		cbKey = null;
		cbMouseButton = null;
		cbScroll = null;
		cbWindowClose = null;
		cbWindowFocus = null;
		cbWindowIconify = null;
		cbWindowPos = null;
		cbWindowRefresh = null;
		cbWindowSize = null;
	}

	public static void preTerminate() {
		GLFW.glfwSetJoystickCallback(null).free();
		GLFW.glfwSetMonitorCallback(null).free();

		cbJoystick = null;
		cbMonitor = null;
	}

	public static void postTerminate() {
		GLFW.glfwSetErrorCallback(null).free();

		cbError = null;
	}
}