package com.indiedev101;

import org.lwjgl.glfw.GLFWDropCallback;

public class CallbackDrop extends GLFWDropCallback {
	@Override
	public void invoke(long window, int count, long names) {
		Log.debug("CallbackDrop: %d %d %d\n", window, count, names);
	}
}