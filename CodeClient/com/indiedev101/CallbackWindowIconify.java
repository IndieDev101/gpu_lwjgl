package com.indiedev101;

import org.lwjgl.glfw.GLFWWindowIconifyCallback;

public class CallbackWindowIconify extends GLFWWindowIconifyCallback {
	@Override
	public void invoke(long window, boolean iconified) {
		//Log.debug("CallbackWindowIconify: %d %d\n", window, iconified);
	}
}