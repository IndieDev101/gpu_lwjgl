package com.indiedev101;

import org.lwjgl.glfw.GLFWErrorCallback;

public class CallbackError extends GLFWErrorCallback {
	@Override
	public void invoke(int error, long description) {
		Log.error("CallackError: %d %d\n", error, description);
	}
}