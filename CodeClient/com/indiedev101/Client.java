package com.indiedev101;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;

public class Client implements Runnable {
	public String[] argv;
	public int tickrate = 66; // how many ticks to perform in one second

	@Override
	public void run() {
		try {
			CallbackManager.preInit();

			if(!GLFW.glfwInit())
				throw new IllegalStateException("Unable to initialize GLFW");

			CallbackManager.postInit();

			GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_VERSION_MAJOR, 3);
			GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_VERSION_MINOR, 2);
			GLFW.glfwWindowHint(GLFW.GLFW_OPENGL_FORWARD_COMPAT, GL11.GL_TRUE);
			GLFW.glfwWindowHint(GLFW.GLFW_OPENGL_PROFILE, GLFW.GLFW_OPENGL_CORE_PROFILE);
			GLFW.glfwWindowHint(GLFW.GLFW_RESIZABLE, GL11.GL_TRUE);
			GLFW.glfwWindowHint(GLFW.GLFW_VISIBLE, GLFW.GLFW_FALSE);
			GLFW.glfwWindowHint(GLFW.GLFW_OPENGL_DEBUG_CONTEXT, GLFW.GLFW_TRUE);
			GLFW.glfwWindowHint(GLFW.GLFW_SAMPLES, 4);

			if(true) {
				App.width = 1280;
				App.height = 720;
			} else {
				App.width = 1920;
				App.height = 1080;
			}
			//GLFW.glfwWindowHint(GLFW.GLFW_RESIZABLE, GL11.GL_FALSE);

			App.window = GLFW.glfwCreateWindow(App.width, App.height, "TestApp", 0, 0);
			if(App.window == 0)
				throw new RuntimeException("Failed to create the glfw window");

			App.resize();
			GPU.resize();

			CallbackManager.postCreateWindow(App.window);

			if(true) {
				long primMon = GLFW.glfwGetPrimaryMonitor();
				GLFWVidMode vidMode = GLFW.glfwGetVideoMode(primMon);
				Log.debug("Resolution: %dx%d\n", vidMode.width(), vidMode.height());
				int x = (vidMode.width() - App.width) / 2;
				int y = (vidMode.height() - App.height) / 2;
				if(true && OS.isWIN() && OS.isDESKTOP()) {
					x = -App.width;
					y = 0;
				}
				GLFW.glfwSetWindowPos(App.window, x, y);
			}

			GLFW.glfwMakeContextCurrent(App.window);
			GLFW.glfwSwapInterval(1);

			GL.createCapabilities();

			GPU.setViewport(0, 0, GPU.width, GPU.height);
			GPU.setClearColor(0x00000000);
			GPU.clear(true, false, false);

			GLFW.glfwShowWindow(App.window);

			GL11.glEnable(GL11.GL_CULL_FACE);

			create();

			long msTotal = 0;
			long tkAccum = 0;
			long tkSimulation = 0;
			long tkStart = GLFW.glfwGetTimerValue();

			while(!App.quit && !GLFW.glfwWindowShouldClose(App.window)) {
				Keyboard.prePollEvents();
				Mouse.prePollEvents();
				GLFW.glfwPollEvents();

				float variableTimeStep = tkSimulation / (float)GLFW.glfwGetTimerFrequency();
				float fixedTimeStep = 1.0f / tickrate;
				long updateTime = (long)(fixedTimeStep * (float)GLFW.glfwGetTimerFrequency());

				long msFixed = (long)((1.0f / (float)tickrate) * 1000.0f);
				tkAccum += tkSimulation;
				long msAccum = (long)(((float)tkAccum / (float)GLFW.glfwGetTimerFrequency()) * 1000.0f);
				int numUpdates = 0;

				if(msAccum >= msFixed) {
					while(msAccum >= msFixed) {
						msTotal += msFixed;

						Timer.timeStep = fixedTimeStep;
						fixedUpdate(fixedTimeStep);

						numUpdates++;
						msAccum -= msFixed;
					}
				}
				tkAccum -= numUpdates * (((float)msFixed / 1000.0f) * (float)GLFW.glfwGetTimerFrequency());

				Timer.timeStep = variableTimeStep;
				variableUpdate(variableTimeStep);

				render();

				GLFW.glfwSwapBuffers(App.window);

				if(true && !App.focus)
					Sys.sleep(50);

				long tkEnd = GLFW.glfwGetTimerValue();
				tkSimulation = tkEnd - tkStart;
				tkStart = tkEnd;
			}

			destroy();

			CallbackManager.preDestroyWindow(App.window);

			GLFW.glfwDestroyWindow(App.window);
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			CallbackManager.preTerminate();
			GLFW.glfwTerminate();
			CallbackManager.postTerminate();
		}
	}

	public void create() {
		GPU.create();
		View.createInstance();
		Draw2D.createInstance();
		Draw3D.createInstance();
		Font.createInstance();
		Grid.createInstance();
		SampleManager.createInstance();
	}

	public void destroy() {
		SampleManager.destroyInstance();
		Grid.destroyInstance();
		Font.destroyInstance();
		Draw3D.destroyInstance();
		Draw2D.destroyInstance();
		View.destroyInstance();
		GPU.destroy();
	}

	public void resize(int width, int height) {
		SampleManager.resizeInstance(width, height);
	}

	public void variableUpdate(float timeStep) {
		SampleManager.variableUpdateInstance(timeStep);
	}

	public void fixedUpdate(float timeStep) {
		SampleManager.fixedUpdateInstance(timeStep);
	}

	public void render() {
		SampleManager.renderInstance();
	}

	public boolean mouseDown(int button, int x, int y) {
		boolean overUI = false;
		return overUI;
	}

	public void mouseMove(int mx, int my, int px, int py) {
	}

	public void mouseUp(int button, int x, int y) {
	}

	public void keyDown(int key) {
	}

	public void keyUp(int key) {
	}

	public static Client instance;

	public static void main(String[] argv) {
		instance = new Client();
		instance.argv = argv;
		instance.run();
		instance = null;
	}
}