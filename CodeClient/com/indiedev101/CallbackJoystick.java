package com.indiedev101;

import org.lwjgl.glfw.GLFWJoystickCallback;

public class CallbackJoystick extends GLFWJoystickCallback {
	@Override
	public void invoke(int joy, int event) {
		Log.debug("CallbackJoystick: %d %d\n", joy, event);
	}
}