package com.indiedev101;

import org.lwjgl.glfw.GLFWMonitorCallback;

public class CallbackMonitor extends GLFWMonitorCallback {
	@Override
	public void invoke(long monitor, int event) {
		Log.debug("CallbackMonitor: %d %d\n", monitor, event);
	}
}