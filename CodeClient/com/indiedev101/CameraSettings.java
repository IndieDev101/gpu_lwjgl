package com.indiedev101;

public class CameraSettings {
	public float moveSpeed = 1;
	public float moveSpeedDec = 0.8f;
	public float moveSpeedInc = 1.2f;
	public float moveMulti = 5.0f;

	public float lookSpeed = 0.03f;
	public float lookMaxPitch = +89.9f;
	public float lookMinPitch = -89.9f;

	public float accelerateDamping = 3;
	public float accelerateEaseIn = 0.5f;
	public float accelerateEaseOut = 1.0f;

	public float deccelerateDamping = 2;
	public float deccelerateEaseIn = 0.8f;
	public float deccelerateEaseOut = 1.0f;

	public float maxForwardsImpulse = 1;
	public float maxBackwardsImpulse = 1;
	public float maxSidewaysInpulse = 1;

	public float strafingDamping = 4;
	public float strafingAccelerateEaseIn = 1.0f;

	public float fastSpeed = 5.0f;
	public float slowSpeed = 5.0f;
}