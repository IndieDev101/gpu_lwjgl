package com.indiedev101;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;

public class Camera {
	public float yaw;
	public float pitch;
	public Vec3f pos = new Vec3f();
	public Vec3f dir = new Vec3f();
	public Vec3f up = new Vec3f(0, 1, 0);
	public Vec3f at = new Vec3f();

	public float impulseFront;
	public float impulseBack;
	public float impulseUp;
	public float impulseDown;
	public float impulseLeft;
	public float impulseRight;

	public float fastScale;
	public float slowScale;

	public float fieldOfView = 70;
	public float aspectRatio;
	public float nearClip = 0.1f;
	public float farClip = 100;

	public Mat4f viewMatrix = new Mat4f();
	public Mat4f projMatrix = new Mat4f();

	public CameraSettings settings = new CameraSettings();

	public Vec2f look = new Vec2f();
	public Vec3f move = new Vec3f();

	public Camera() {
		reset();
		load();
	}

	public void reset(float x, float y, float z, float yaw, float pitch) {
		pos.set(x, y, z);
		this.yaw = yaw;
		this.pitch = pitch;
	}

	public void reset() {
		reset(-1.0f, 1.7f, -1.0f, 45.0f, -35.264389f);
	}

	public void update(float timeStep) {
		float moveValueFront = Keyboard.isKeyDown(Keyboard.KeyW) ? 1 : 0;
		float moveValueBack = Keyboard.isKeyDown(Keyboard.KeyS) ? 1 : 0;
		float moveValueUp = Keyboard.isKeyDown(Keyboard.KeyE) ? 1 : 0;
		float moveValueDown = Keyboard.isKeyDown(Keyboard.KeyQ) ? 1 : 0;
		float moveValueLeft = Keyboard.isKeyDown(Keyboard.KeyA) ? 1 : 0;
		float moveValueRight = Keyboard.isKeyDown(Keyboard.KeyD) ? 1 : 0;
		float moveValueFast = Keyboard.isKeyDown(Keyboard.KeyLeftAlt) ? 1 : 0;
		float moveValueSlow = Keyboard.isKeyDown(Keyboard.KeyLeftShift) ? 1 : 0;
		float moveWheel = Mouse.wheel;
		float lookMoveX = -Mouse.moveX;
		float lookMoveY = -Mouse.moveY;
		float lookRatio = 720.0f / (float)App.height;
		boolean lookEnabled = Mouse.isGrabbed();

		if(Keyboard.isKeyPress(Keyboard.KeyC))
			save();
		if(Keyboard.isKeyPress(Keyboard.KeyV))
			load();

		look.set(0, 0);
		move.set(0, 0, 0);

		if(lookEnabled) {
			look.x = lookMoveX * lookRatio * settings.lookSpeed;
			look.y = lookMoveY * lookRatio * settings.lookSpeed;

			yaw += look.x;
			pitch += look.y;
		}

		if(pitch < settings.lookMinPitch)
			pitch = settings.lookMinPitch;
		else if(pitch > settings.lookMaxPitch)
			pitch = settings.lookMaxPitch;

		yaw = Mathf.getAngleBetweenPosNeg180(yaw);
		pitch = Mathf.getAngleBetweenPosNeg180(pitch);

		Mathf.setDirectionFromYawAndPitch(dir, yaw, pitch);

		float moveScale = 1;

		if(moveWheel < 0) {
			settings.moveSpeed *= settings.moveSpeedDec;
		} else if(moveWheel > 0) {
			settings.moveSpeed *= settings.moveSpeedInc;
		}

		if(moveValueFront != 0) {
			impulseFront += timeStep * settings.accelerateDamping;
			if(impulseFront > settings.maxForwardsImpulse)
				impulseFront = settings.maxForwardsImpulse;
			move.z += Mathf.ease(impulseFront, settings.accelerateEaseIn, settings.accelerateEaseOut);
		} else {
			impulseFront -= timeStep * settings.deccelerateDamping;
			if(impulseFront < 0)
				impulseFront = 0;
			move.z += Mathf.ease(impulseFront, settings.deccelerateEaseIn, settings.deccelerateEaseOut);
		}

		if(moveValueBack != 0) {
			impulseBack += timeStep * settings.accelerateDamping;
			if(impulseBack > settings.maxBackwardsImpulse)
				impulseBack = settings.maxBackwardsImpulse;
			move.z -= Mathf.ease(impulseBack, settings.accelerateEaseIn, settings.accelerateEaseOut);
		} else {
			impulseBack -= timeStep * settings.deccelerateDamping;
			if(impulseBack < 0)
				impulseBack = 0;
			move.z -= Mathf.ease(impulseBack, settings.deccelerateEaseIn, settings.deccelerateEaseOut);
		}

		if(moveValueUp != 0) {
			impulseUp += timeStep * settings.accelerateDamping;
			if(impulseUp > 1)
				impulseUp = 1;
			move.y += Mathf.ease(impulseUp, settings.accelerateEaseIn, settings.accelerateEaseOut);
		} else {
			impulseUp -= timeStep * settings.deccelerateDamping;
			if(impulseUp < 0)
				impulseUp = 0;
			move.y += Mathf.ease(impulseUp, settings.deccelerateEaseIn, settings.deccelerateEaseOut);
		}

		if(moveValueDown != 0) {
			impulseDown += timeStep * settings.accelerateDamping;
			if(impulseDown > 1)
				impulseDown = 1;
			move.y -= Mathf.ease(impulseDown, settings.accelerateEaseIn, settings.accelerateEaseOut);
		} else {
			impulseDown -= timeStep * settings.deccelerateDamping;
			if(impulseDown < 0)
				impulseDown = 0;
			move.y -= Mathf.ease(impulseDown, settings.deccelerateEaseIn, settings.deccelerateEaseOut);
		}

		if(moveValueRight != 0 && moveValueLeft == 0) {
			impulseLeft -= timeStep * settings.strafingDamping;
		}

		if(moveValueLeft != 0 && moveValueRight == 0) {
			impulseRight -= timeStep * settings.strafingDamping;
		}

		if(moveValueRight != 0) {
			impulseRight += timeStep * settings.accelerateDamping;
		} else {
			impulseRight -= timeStep * settings.deccelerateDamping;
		}

		if(moveValueLeft != 0) {
			impulseLeft += timeStep * settings.accelerateDamping;
		} else {
			impulseLeft -= timeStep * settings.deccelerateDamping;
		}

		impulseRight = Mathf.clamp(impulseRight, 0.0f, settings.maxSidewaysInpulse);
		impulseLeft = Mathf.clamp(impulseLeft, 0.0f, settings.maxSidewaysInpulse);

		if(moveValueRight != 0) {
			move.x += Mathf.ease(impulseRight, settings.strafingAccelerateEaseIn, settings.accelerateEaseOut);
		} else {
			move.x += Mathf.ease(impulseRight, settings.deccelerateEaseIn, settings.deccelerateEaseOut);
		}

		if(moveValueLeft != 0) {
			move.x -= Mathf.ease(impulseLeft, settings.strafingAccelerateEaseIn, settings.accelerateEaseOut);
		} else {
			move.x -= Mathf.ease(impulseLeft, settings.deccelerateEaseIn, settings.deccelerateEaseOut);
		}

		float fastMulti = 1.0f;
		if(moveValueFast != 0) {
			fastScale += timeStep * settings.fastSpeed;
			if(fastScale > 0.5f)
				fastScale = 0.5f;
		} else {
			fastScale -= timeStep * settings.fastSpeed;
			if(fastScale < 0.0f)
				fastScale = 0.0f;
		}
		if(fastScale > 0.0f) {
			fastMulti = 1.0f + fastScale;
			moveScale *= fastMulti;
		}

		float slowMulti = 1.0f;
		if(moveValueSlow != 0) {
			slowScale += timeStep * settings.slowSpeed;
			if(slowScale > 0.5f)
				slowScale = 0.5f;
		} else {
			slowScale -= timeStep * settings.slowSpeed;
			if(slowScale < 0.0f)
				slowScale = 0.0f;
		}
		if(slowScale > 0.0f) {
			slowMulti = 1.0f - slowScale;
			moveScale *= slowMulti;
		}

		float moveLength = Mathf.clamp(move.length(), 0, 1);
		if(moveLength > Mathf.EPSILON) {
			move.normalize();
			move.mul(moveLength);
		}

		float moveSpeed = settings.moveMulti * fastMulti * settings.moveSpeed;

		move.mul(moveSpeed);
		move.mul(moveScale);

		if(move.x != 0.0f) {
			float inc = move.x * timeStep;
			pos.x -= dir.z * inc;
			pos.z += dir.x * inc;
		}

		if(move.y != 0.0f) {
			float inc = move.y * timeStep;
			pos.y += inc;
		}

		if(move.z != 0.0f) {
			float inc = move.z * timeStep;
			pos.x += dir.x * inc;
			pos.y += dir.y * inc;
			pos.z += dir.z * inc;
		}

		at.add(pos, dir);

		MathUtils.setLookAt(pos.x, pos.y, pos.z, at.x, at.y, at.z, up.x, up.y, up.z, viewMatrix);

		aspectRatio = (float)GPU.width / (float)GPU.height;

		MathUtils.setPersp(fieldOfView, aspectRatio, nearClip, farClip, projMatrix);
	}

	public void load() {
		FileReader reader = null;
		try {
			File file = new File("camera.txt");
			reader = new FileReader(file);
			char[] buffer = new char[(int)file.length()];
			reader.read(buffer);
			String string = new String(buffer);
			String[] temp = string.split(" ");
			pos.x = Float.parseFloat(temp[0]);
			pos.y = Float.parseFloat(temp[1]);
			pos.z = Float.parseFloat(temp[2]);
			pitch = Float.parseFloat(temp[3]);
			yaw = Float.parseFloat(temp[4]);
		} catch(FileNotFoundException e) {
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			if(reader != null) {
				try {
					reader.close();
				} catch(IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void save() {
		try {
			File file = new File("camera.txt");
			FileWriter writer = new FileWriter(file);
			writer.write(String.format("%f %f %f %f %f", pos.x, pos.y, pos.z, pitch, yaw));
			writer.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}

	public static Camera instance;

	public static void createInstance() {
		if(instance == null)
			instance = new Camera();
	}

	public static void destroyInstance() {
		if(instance != null)
			instance = null;
	}

	public static void updateInstance(float timeStep) {
		if(instance != null)
			instance.update(timeStep);
	}
}