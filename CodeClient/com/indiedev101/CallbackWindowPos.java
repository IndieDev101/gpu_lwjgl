package com.indiedev101;

import org.lwjgl.glfw.GLFWWindowPosCallback;

public class CallbackWindowPos extends GLFWWindowPosCallback {
	@Override
	public void invoke(long window, int xpos, int ypos) {
		//Log.debug("CallbackWindowPos: %d %d %d\n", window, xpos, ypos);
	}
}