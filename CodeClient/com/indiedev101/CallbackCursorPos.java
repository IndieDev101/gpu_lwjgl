package com.indiedev101;

import org.lwjgl.glfw.GLFWCursorPosCallback;

public class CallbackCursorPos extends GLFWCursorPosCallback {
	@Override
	public void invoke(long window, double xpos, double ypos) {
		//Log.debug("CallbackCursorPos: %d %f %f\n", window, xpos, ypos);

		Mouse.currX = App.win2gpuX(xpos);
		Mouse.currY = App.win2gpuY(ypos);
		if(Mouse.iGrabbed++ == 0) {
			Mouse.prevX = Mouse.currX;
			Mouse.prevY = Mouse.currY;
		}
		Mouse.moveX = Mouse.currX - Mouse.prevX;
		Mouse.moveY = Mouse.currY - Mouse.prevY;

		Client.instance.mouseMove(Mouse.moveX, Mouse.moveY, Mouse.currX, Mouse.currY);
	}
}