package com.indiedev101;

import org.lwjgl.glfw.GLFWWindowRefreshCallback;

public class CallbackWindowRefresh extends GLFWWindowRefreshCallback {
	@Override
	public void invoke(long window) {
		//Log.debug("CallbackWindowRefresh: %d\n", window);
	}
}