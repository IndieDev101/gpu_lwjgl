package com.indiedev101;

import org.lwjgl.glfw.GLFWFramebufferSizeCallback;

public class CallbackFramebufferSize extends GLFWFramebufferSizeCallback {
	@Override
	public void invoke(long window, int width, int height) {
		//Log.debug("CallbackFramebufferSize: %d %d %d\n", window, width, height);

		GPU.resize();
		Client.instance.resize(width, height);
	}
}