in vec2 iPosition;
in vec4 iColor;
in vec2 iTexCoord;

out vec4 vColor;
out vec2 vTexCoord;

uniform mat4 uScreenMatrix;

void main() {
	gl_Position = uScreenMatrix * vec4(iPosition, 0, 1);
	vColor = iColor;
	vTexCoord = iTexCoord;
}