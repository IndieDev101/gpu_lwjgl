in vec3 iPosition;
in vec4 iColor;
in vec2 iTexCoord;

out vec4 vColor;
out vec2 vTexCoord;

uniform mat4 uViewMatrix;
uniform mat4 uProjMatrix;

void main() {
	gl_Position = uProjMatrix * uViewMatrix * vec4(iPosition, 1.0);
	vColor = iColor;
	vTexCoord = iTexCoord;
}
